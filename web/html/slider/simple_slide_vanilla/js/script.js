document.addEventListener('DOMContentLoaded', function() {

    document.getElementById('checkbox').addEventListener('change',function(){
        setInterval(function () {
            moveRight(slideUl);
        }, 3000);
    });

    var slide = document.getElementById('slider');
    var slideUl = slide.querySelector('ul');
    var slideLi = slideUl.querySelector('li');
    var prev = document.querySelector('.control_prev');
    var next = document.querySelector('.control_next');

    var slideCount = slideLi.length;
    var slideWidth = slideLi.clientWidth;
    var slideHeight = slideLi.clientHeight;
    var sliderUlWidth = slideCount * slideWidth;

    multiStyle(slide,{width: slideWidth, height: slideHeight});
    multiStyle(slideUl,{width: sliderUlWidth, marginLeft: - slideWidth});

    $('#slider ul li:last-child').prependTo('#slider ul');


    prev.addEventListener('click',moveLeft);
    next.addEventListener('click',moveRight);


});

jQuery(document).ready(function ($) {

    // $('#checkbox').change(function(){
    //     setInterval(function () {
    //         moveRight();
    //     }, 3000);
    // });

    // var slideCount = $('#slider ul li').length;
    // var slideWidth = $('#slider ul li').width();
    // var slideHeight = $('#slider ul li').height();
    // var sliderUlWidth = slideCount * slideWidth;

    // $('#slider').css({ width: slideWidth, height: slideHeight });
    //
    // $('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });

    // $('#slider ul li:last-child').prependTo('#slider ul');



    // $('a.control_prev').click(function () {
    //     moveLeft();
    // });
    //
    // $('a.control_next').click(function () {
    //     moveRight();
    // });

});

function multiStyle(ele, propertyObject){
    for (var property in propertyObject) {
        ele.style[property] = propertyObject[property];
    }
}
function myMove() {
    var elem = document.getElementById("myAnimation");
    var pos = 0;
    var id = setInterval(frame, 10);
    function frame() {
        if (pos == 350) {
            clearInterval(id);
        } else {
            pos++;
            elem.style.top = pos + 'px';
            elem.style.left = pos + 'px';
        }
    }
}
function moveLeft(ele) {
    console.log(ele);
    var pos = 0;
    var id = setInterval(frame, 10);
    function frame() {
        if (pos == 350) {
            clearInterval(id);
        } else {
            pos++;
            ele.style.left = pos + 'px';
        }
    }
    // $('#slider ul').animate({
    //     left: + slideWidth
    // }, 200, function () {
    //     $('#slider ul li:last-child').prependTo('#slider ul');
    //     $('#slider ul').css('left', '');
    // });
};

function moveRight(ele) {
    console.log(ele);
    // $('#slider ul').animate({
    //     left: - slideWidth
    // }, 200, function () {
    //     $('#slider ul li:first-child').appendTo('#slider ul');
    //     $('#slider ul').css('left', '');
    // });
};