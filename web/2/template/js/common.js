var responsive;

function setResponsive() {
    if ($('div#media-479').css('display') == 'block') responsive = 1;
    else if ($('div#media-768').css('display') == 'block') responsive = 2;
    else if ($('div#media-1024').css('display') == 'block') responsive = 3;
    else if ($('div#media-1366').css('display') == 'block') responsive = 4;
    else responsive = 4;
}

$(window).on('resize', debounce(function(e) {
    card_move(responsive);
    setResponsive();
},100));

$(window).on('load', function () {

    setResponsive();

    $('.card').addClass('active');
    $('.card_inner').each(function(i,ele){
        $(ele).attr({"num":i});
    });
    card_move(responsive);

    var tag_list = {
        "select" : [],
        "design":[
            "photoshop",
            "illustrator",
        ],
        "coding":[
            "html & css",
            "javascript & jquery",
            "php",
            "mvc pattern"
        ]
    };


    var typed = new Typed(".type",{
        strings:['SOURCE'],
        typeSpeed: 100,
        backSpeed: 50,
        loop: false
    });

    //modal
    (function(){
        $('.header .menu').on('click','div',function(){
            var tag_name, tag_ul, select_li;
            tag_name = $(this).text();
            tag_ul = $('.modal .tag_list');


            $('.modal').addClass('active');
            tag_ul.empty();

            for(var i = 0; i < tag_list[tag_name].length; i++){
                tag_ul.append("<li>"+ tag_list[tag_name][i]+ "</li>")
            }
            for(var value of tag_list.select){ //tag_list.select에서 선택한 텍스트에 불들어오게 하기
                select_li = $('.modal li:contains("'+ value +'")');
                select_li.addClass('on');
            }
        });

        $('.modal .m_close').on('click',function () {
            $(this).parents('.modal').removeClass('active');
        });
        $('.modal').on('click',function(){
            $(this).removeClass('active');
        });
        $('.modal .m_wrap').on('click',function(e){
            // e.preventDefault();
            e.stopPropagation();
        });

    })();

    $('.tag_list').on('click',"li",function(){

        var select_value = $(this).text();
        var select_li = $('.tag_list li:contains("'+ select_value +'")');
        var has = select_li.hasClass('on');

        if(!has){//
            select_li.addClass('on');
            if(tag_list.select.indexOf(select_value) != -1){
                console.log('같음');
            }else{
                tag_list.select.push(select_value);// tag_list.select에 지금 클릭한 값이 없다면 값을 추가해라

            }
        }else {
            select_li.removeClass('on');
            for(var i = 0; i < tag_list.select.length; i++){
                if(tag_list.select[i].indexOf(select_value) != -1){
                    tag_list.select.splice(i, 1);// tag_list.select에 지금 클릭한 값이 있다면 값을 삭제해라
                }
            }
        }


        // select_li.toggleClass('on');

        $('.card').removeClass('active');

        $('.card').each(function(index,ele){
            var active = $(ele).find('.on');
            active.parents('.card').addClass('active');
        });

        card_move(responsive);
    });


});

function card_move(number){
    var card_wrap, card, select_card;
    card_wrap = $('.con_wrap .inner');
    card = $('.card');
    select_card = $('.con_wrap .active');

    start_move(card_wrap,select_card,number);
}

function start_move(wrap,select,num){
    var x, y, select_y, y_data,h_compare,wrap_height;
    x = 0;
    y = 0;
    select_y = [];
    y_data = [];
    h_compare = [];
    wrap_height = [];

    for (var i = 0; i < select.length; i++){
        var a = parseInt(i%num); // 000111222
        var b = parseInt(i/num); //012012012

        x = (i % num) * (parseInt(select.outerWidth()));
        y = parseInt(i/num) * (parseInt(select.outerHeight()));
        if(!select_y[a])select_y[a] = [];
        select_y[a][b] = (parseInt($(select[i]).outerHeight()));
        if(!y_data[a])y_data[a] = [];

        if(i < num){
            y_data[a][b] = 0;
        }else{
            y_data[a][b] = (y_data[a][b-1])+(select_y[a][b-1]);
            h_compare[a] = y_data[a][b];
        }
        select[i].style.top = y_data[a][b] + 'px';
        select[i].style.left = x + 'px';
        // $cardHeight = y;
    }
    // console.log(select_y);
    // console.log(y_data);

    // 숫자를 크기순으로 소트할 때, 내부적으로 필요한 함수
    function compNumber(a, b) {
        return a - b;
    }
    // 숫자를 역순으로 정렬할 때, 내부적으로 필요한 함수
    function compNumberReverse(a, b) {
        return b - a;
    }

    wrap_height = h_compare.sort(compNumberReverse);

    // wrap.css('height',$cardHeight + select.outerHeight() ); //content의 높이값 구해서 넣어주기.
    wrap.css('height', 500+wrap_height[0]); //content의 높이값 구해서 넣어주기.

}

function debounce(fn, delay) {
    var timer = null;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            fn.apply(context, args);
        }, delay);
    };

};
