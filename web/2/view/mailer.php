<!-- section -->
<h2 class=blind>연락</h2>
<section class="section contact">
<div class="sec_wrap">
    <h1>Contact</h1>
    <p>I am not currently taking on new clients. However, I hope to be back accepting new projects this year.</p>
    <div class="contact_list">
        <ul>
            <li>
                <h4>기업개요</h4>
                <dl>
                    <dt>회사명</dt>
                    <dd>주식회사 오픈필드</dd>
                </dl>
                <dl>
                    <dt>분야</dt>
                    <dd>주식회사 오픈필드</dd>
                </dl>
                <dl>
                    <dt>git</dt>
                    <dd>주식회사 오픈필드</dd>
                </dl>
            </li>
            <li>
                <h4>연락처</h4>
                <dl>
                    <dt>핸드폰</dt>
                    <dd>010-9954-4545</dd>
                </dl>
                <dl>
                    <dt>이메일</dt>
                    <dd>mj037e@gmail.com</dd>
                </dl>
            </li>
        </ul>
    </div>

    <div class="mail_container cover">
        <div class="mail_inner cover_inner">
            <form action="../../function/mail_process.php" method="POST">
                <ul>
                    <li>
                        <label for="title">Title</label>
                        <input type="text" id="title" name="title" placeholder="title">
                    </li>
                    <li>
                        <label for="name">Full Name</label>
                        <input type="text" id="name" name="your_name" placeholder="your_name">
                    </li>
                    <li>
                        <label for="email">Email</label>
                        <input type="text" id="email" name="your_email" placeholder="your_email">
                    </li>
                    <li>
                        <label for="description">Massage</label>
                        <textarea id="description" name="description" placeholder="description"></textarea>
                    </li>
                </ul>
                <button type="submit" class="sand">sand massage</button>
            </form>
        </div>
    </div>

</div>

</section>
