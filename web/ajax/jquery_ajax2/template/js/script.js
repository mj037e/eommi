$(document).ready(function() {

    var list = {
        route : 'view/',
        menu : [
            {
                'page_name' : 'page1',
                'title' : '일정표',
                'image_number' : 0,
                'file_name': '.png',
            },
            {
                'page_name' : 'page2',
                'title' : '단원연락처',
                'image_number' : 0,
                'file_name': '.png',
            },
            {
                'page_name' : 'page3',
                'title' : '강의자료',
                'image_number' : 0,
                'file_name': '.png',
            },
            {
                'page_name' : 'page4',
                'title' : '안전관리지침',
                'image_number' : 0,
                'file_name': '.png',
            },
            {
                'page_name' : 'page5',
                'title' : '참고자료',
                'image_number' : 0,
                'file_name': '.png',
            },
            {
                'page_name' : 'page6',
                'title' : '포토갤러리',
                'image_number' : 0,
                'file_name': '.png',
            },
        ],
    };


    for(var i = 0; i < list.menu.length; i++) {
        j = i + 1;
        if (j < 10) {
            j = '0' + j
        }
        $('.menu ul').append('<li><a href="#!'+list.menu[i].page_name+'">' + list.menu[i].title + '</a></li>');
    }
//     //1.html 등 의 text 문서 읽기
    $('.menu li').each(function(i){
        $(this).on('click',function () {
            makeRequest(list.route+list.menu[i].page_name+'.html');
        });
    })
    function location_hash(){
        console.log(list.route+location.hash.substr(2)+'.html');
        if(location.hash){
            makeRequest(list.route+location.hash.substr(2)+'.html');
        }else{
            makeRequest('./view/main.html');
        }

    }
    function makeRequest(url) {
        console.log(url);
        $("#container").load(url, function(responseTxt, statusTxt, xhr){
            if(statusTxt == "success")
                console.log("External content loaded successfully!");
            if(statusTxt == "error")
                console.log("Error: " + xhr.status + ": " + xhr.statusText);
        });
        // $("#container").hide().load(url, function () {
        //     $(this).fadeIn(); // fade effect
        // });
    }
//
    window.addEventListener("hashchange",function(){
        location_hash();
    });
// //        if(list.menu[i].image_number){
// //            for(var i = 0; i < list.menu.image_number; i++){
// //                j = i+1;
// //                if(j < 10){
// //                    j = '0'+j
// //                }
// //                $('.images_list')
// //                    .append(
// //                        $('<div>'+j+'</div>'
// //                        ).css({
// //                            'width':'100px',
// //                            'height':'100px',
// //                            'background-image':'url(template/data/page1/'+j+'.png)',
// //                            'background-size':'cover'
// //                        }));
// //            }
// //        }
//
//

});
