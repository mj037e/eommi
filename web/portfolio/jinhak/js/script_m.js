var handle_event= (function($){

    var has_on = function(){
        var has = $(this).hasClass('active');
        if(!has){
            $(this).addClass('active');
        }else{
            $(this).removeClass('active');
        }
    }

    var off = function(){
        $(this).parent('.recent').siblings().children('.re_title').removeClass('active');
    }
    var remove_val = function(){
        $(this).parent('a').siblings('input').val("")
    }

    return {
        search:function(){
            $('.search_btn').on('click',has_on);
            $('.times').on('click',remove_val)
        },
        accordion:function(){
            $('.re_title').on('click',has_on);
            $('.re_title').on('click',off);
            $('.s_lnb>li').on('click',has_on);
            $('.sub>li').on('click',function(e){
                 e.stopPropagation();
            })

        },
        snb:function(){
            $('.menu_btn').on('click',function(){
                $('.snb').addClass('active');
            });
            $('.snb').on('click',function(){
                $(this).removeClass('active');
            });
            $('.snb .exit').on('click',function(){
                $('.snb').removeClass('active');
            });
            $('.snb_inner').on('click',function(e){
                 e.stopPropagation();
            })
        }

    }
    
}(jQuery)); 
    
$(document).ready(function(){

    handle_event.search();
    handle_event.accordion();
    handle_event.snb();

    $('.bxslider').bxSlider({
        buildPager: function(slideIndex){
            return ""
        },
        speed: 500,
        autoHover: true, 
        auto: true,
        autoControls: true
    });          
});