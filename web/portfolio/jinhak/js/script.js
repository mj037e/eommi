var setting = (function($){

    return{
        h_sub_w : function(){
            var sub = $('.sub')
            var s_w  =[];

            for(var i = 0; i < $('.sub').length; i++){
                 var subLi  =$('.sub').eq(i).children();
                 s_w[i] = [];

                 for(var j = 0; j < subLi.length; j++){
                    var l_w = subLi.eq(j).width();
                    s_w[i]  = Number(s_w[i])+Number(l_w.toFixed(2));
                    //console.log(l_w);
                 }
                 sub.eq(i).css('width',s_w[i]+1);
            }
        },
        bxslider : function(){
            $('.bxslider').bxSlider({
                buildPager: function(slideIndex){
                    return ""
                },
                speed: 500,
                autoHover: true, 
                auto: true,
                autoControls: true
            });          
        }

    }

}(jQuery));

var mouse_event= (function($){
    var btn_on = function(){
        $(this).siblings().removeClass('on');
        $(this).addClass('on');
    }
    var btn_off = function(){
        $(this).removeClass('on')
    }

    var tab = function(){
        $(this).next().show();
        $(this).addClass('active');
        $(this).parent('.re_tab').siblings().children('.ta_content').hide();
        $(this).parent('.re_tab').siblings().children('.ta_title').removeClass('active');
    }
    var on=function(){
        $(this).addClass('active');
    }
    var off=function(e){
        e.stopPropagation()
        $(this).parent('li').removeClass('active');
    }

    return {
        header : function(){
            $('.h_lnb>li').on('mouseenter',btn_on);
            $('.h_lnb>li').on('mouseleave',btn_off);
        },
        main_tab : function(){
            $('.re_tab .ta_title').on('click',tab);
            $('.re_tab:nth-child(1) .ta_title').trigger('click');
        },
        snb:function(){
            $('.snb li').eq(2).addClass('on');
            $('.snb li').on('click',btn_on);
        },
        search:function(){
            $('.search').on('click',on);
            $('.search').on('click',btn_on);
            $('.exit').on('click',off);
        }

    }
    
}(jQuery)); 
    
$(document).ready(function(){

    setting.h_sub_w();
    setting.bxslider();

    mouse_event.main_tab();
    mouse_event.header();
    mouse_event.snb();
    mouse_event.search();

});