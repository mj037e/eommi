document.addEventListener("DOMContentLoaded", function(event) {
var nowScroll, body, header, top_button, main, video, arrow, sentence_01, sentence_02, sentence_03, sentence_04, eraser, bloc_01, bloc_02, bloc_03, bloc_04, skill_01, skill_02, skill_03, skill_04, skill_05, skill_06, skill_07, skill_08, skill_09, skill_10, smoothScrollTo;

nowScroll = 0;
body = document.getElementsByTagName('body')[0];
header = querySelector('.header');
top_button = querySelector('.top');
main = querySelector('.main');
video = querySelector('.video');
arrow = querySelector('.arrow');
sentence_01 = querySelector('.bloc1 .sentence');
sentence_02 = querySelector('.bloc2 .sentence');
sentence_03 = querySelector('.bloc3 .sentence');
sentence_04 = querySelector('.bloc4 .sentence');
eraser = querySelector('.section .eraser');
bloc_01 = querySelector('.bloc1');
bloc_02 = querySelector('.bloc2');
bloc_03 = querySelector('.bloc3');
bloc_04 = querySelector('.bloc4');
skill_01 = querySelector('.skill1');
skill_02 = querySelector('.skill2');
skill_03 = querySelector('.skill3');
skill_04 = querySelector('.skill4');
skill_05 = querySelector('.skill5');
skill_06 = querySelector('.skill6');
skill_07 = querySelector('.skill7');
skill_08 = querySelector('.skill8');
skill_09 = querySelector('.skill9');
skill_10 = querySelector('.skill10');

window.addEventListener('scroll', function(e) {
    
    nowScroll = window.pageYOffset;

    /*top*/
    if(nowScroll > 0 && nowScroll <= 3800){multiStyle(top_button,{'opacity':0});
    }else if(nowScroll > 3800 && nowScroll <= 3850){multiStyle(top_button,{'opacity':0.2});
    }else if(nowScroll > 3850 && nowScroll <= 3900){multiStyle(top_button,{'opacity':0.4});
    }else if(nowScroll > 3900 && nowScroll <= 3950){multiStyle(top_button,{'opacity':0.6});
    }else if(nowScroll > 3950 && nowScroll <= 4000){multiStyle(top_button,{'opacity':0.8});
    }else if(nowScroll > 4000 && nowScroll <= 7260){multiStyle(top_button,{'opacity':1});
    }      

    /*body*/
    if(nowScroll > 0 && nowScroll <= 4200){multiStyle(body,{'background-color':'rgb(121, 13, 70)'});
    }else if(nowScroll > 4200 && nowScroll <= 7260){multiStyle(body,{'background-color':'rgb(29,29,29)'});
    } 

//    if(nowScroll > 0 && nowScroll <= 4600){
//        header.classList.add('active');
//    }else if(nowScroll > 4600 && nowScroll <= 7260){
//        header.classList.remove('active');
//    }    

    /*home*/
    if(nowScroll > 0 && nowScroll <= 4200){multiStyle(main,{'display':'block'});
    }else if(nowScroll > 4200 && nowScroll <= 7260){multiStyle(main,{'display':'none'});
    } 

    /*video*/
    if(nowScroll > 0 && nowScroll <= 500){multiStyle(video,{'opacity':1});
    }else if(nowScroll > 500 && nowScroll <= 600){multiStyle(video,{'opacity':0.8});
    }else if(nowScroll > 600 && nowScroll <= 700){multiStyle(video,{'opacity':0.6});
    }else if(nowScroll > 700 && nowScroll <= 800){multiStyle(video,{'opacity':0.4});
    }else if(nowScroll > 800 && nowScroll <= 900){multiStyle(video,{'opacity':0.4});
    }else if(nowScroll > 900 && nowScroll <= 4500){multiStyle(video,{'opacity':0});
    }
    
    /*scroll*/
    if(nowScroll > 0 && nowScroll <= 500){multiStyle(arrow,{'opacity':1});
    }else if(nowScroll > 500 && nowScroll <= 600){multiStyle(arrow,{'opacity':0.8});
    }else if(nowScroll > 600 && nowScroll <= 700){multiStyle(arrow,{'opacity':0.6});
    }else if(nowScroll > 700 && nowScroll <= 800){multiStyle(arrow,{'opacity':0.4});
    }else if(nowScroll > 800 && nowScroll <= 900){multiStyle(arrow,{'opacity':0.4});
    }else if(nowScroll > 900 && nowScroll <= 4500){multiStyle(arrow,{'opacity':0});
    }
    
    /*arrow버튼*/
    if(nowScroll > 0 && nowScroll <= 900){multiStyle(arrow,{'visibility':'visible'});
    }else if(nowScroll > 900 && nowScroll <= 4500){multiStyle(arrow,{'visibility':'visible'});
    }


    /*bloc1 sentence*/
    if(nowScroll > 0 && nowScroll <= 1300){multiStyle(sentence_01,{'opacity':0});
    }else if(nowScroll > 1300 && nowScroll <= 1400){multiStyle(sentence_01,{'opacity':0.2});
    }else if(nowScroll > 1400 && nowScroll <= 1500){multiStyle(sentence_01,{'opacity':0.4});
    }else if(nowScroll > 1500 && nowScroll <= 1600){multiStyle(sentence_01,{'opacity':0.6});
    }else if(nowScroll > 1600 && nowScroll <= 1700){multiStyle(sentence_01,{'opacity':0.8});
    }else if(nowScroll > 1800 && nowScroll <= 4000){multiStyle(sentence_01,{'opacity':1});
    }else if(nowScroll > 4000 && nowScroll <= 4500){multiStyle(sentence_01,{'opacity':0});
    }


    /*bloc2 sentence*/
    if(nowScroll > 0 && nowScroll <= 2300){multiStyle(sentence_02,{'opacity':0});
    }else if(nowScroll > 2300 && nowScroll <= 2400){multiStyle(sentence_02,{'opacity':0.2});
    }else if(nowScroll > 2400 && nowScroll <= 2500){multiStyle(sentence_02,{'opacity':0.4});
    }else if(nowScroll > 2500 && nowScroll <= 2600){multiStyle(sentence_02,{'opacity':0.6});
    }else if(nowScroll > 2600 && nowScroll <= 2700){multiStyle(sentence_02,{'opacity':0.8});
    }else if(nowScroll > 2700 && nowScroll <= 4500){multiStyle(sentence_02,{'opacity':1});
    }

    /*bloc3 sentence*/
    if(nowScroll > 0 && nowScroll <= 2800){multiStyle(sentence_03,{'opacity':0});
    }else if(nowScroll > 2800 && nowScroll <= 2900){multiStyle(sentence_03,{'opacity':0.25});
    }else if(nowScroll > 2900 && nowScroll <= 3000){multiStyle(sentence_03,{'opacity':0.5});
    }else if(nowScroll > 3000 && nowScroll <= 3100){multiStyle(sentence_03,{'opacity':0.75});
    }else if(nowScroll > 3100 && nowScroll <= 4500){multiStyle(sentence_03,{'opacity':1});
    }

    /*bloc4 sentence*/
    if(nowScroll > 0 && nowScroll <= 2900){multiStyle(sentence_04,{'bottom':'60px','opacity':'0'});
    }else if(nowScroll > 2900 && nowScroll <= 3000){multiStyle(sentence_04,{'bottom':'60px','opacity':'0.2'});        
    }else if(nowScroll > 3000 && nowScroll <= 3100){multiStyle(sentence_04,{'bottom':'60px','opacity':'0.4'});        
    }else if(nowScroll > 3100 && nowScroll <= 3200){multiStyle(sentence_04,{'bottom':'50px','opacity':'0.6'});        
    }else if(nowScroll > 3200 && nowScroll <= 3300){multiStyle(sentence_04,{'bottom':'40px','opacity':'0.8'});        
    }else if(nowScroll > 3300 && nowScroll <= 3400){multiStyle(sentence_04,{'bottom':'30px','opacity':'1'});        
    }else if(nowScroll > 3400 && nowScroll <= 3500){multiStyle(sentence_04,{'bottom':'20px','opacity':'1'});        
    }else if(nowScroll > 3500 && nowScroll <= 3600){multiStyle(sentence_04,{'bottom':'10px','opacity':'1'});        
    }else if(nowScroll > 3600 && nowScroll <= 4100){multiStyle(sentence_04,{'bottom':'0px','opacity':'1'});        
    }else if(nowScroll > 4100 && nowScroll <= 4500){multiStyle(sentence_04,{'bottom':'0px','opacity':'0'});        
    }

    /*bloc eraser*/
    if(nowScroll > 0 && nowScroll <= 3600){multiStyle(eraser,{'opacity':'0','transform':'rotate(-30deg) translate(0%,-300%) scale(2)'});
    }else if(nowScroll > 3600 && nowScroll <= 3700){multiStyle(eraser,{'opacity':'1','transform':'rotate(-30deg) translate(0%,-300%) scale(2)'});        
    }else if(nowScroll > 3700 && nowScroll <= 3800){multiStyle(eraser,{'opacity':'1','transform':'rotate(-30deg) translate(0%,-260%) scale(2)'});        
    }else if(nowScroll > 3800 && nowScroll <= 3900){multiStyle(eraser,{'opacity':'1','transform':'rotate(-30deg) translate(0%,-220%) scale(2)'});        
    }else if(nowScroll > 3900 && nowScroll <= 4000){multiStyle(eraser,{'opacity':'1','transform':'rotate(-30deg) translate(0%,-180%) scale(2)'});        
    }else if(nowScroll > 4000 && nowScroll <= 4100){multiStyle(eraser,{'opacity':'1','transform':'rotate(-30deg) translate(0%,-140%) scale(2)'});        
    }else if(nowScroll > 4100 && nowScroll <= 4200){multiStyle(eraser,{'opacity':'1','transform':'rotate(-30deg) translate(0%,-100%) scale(2)'});        
    }else if(nowScroll > 4200 && nowScroll <= 4300){multiStyle(eraser,{'opacity':'0','transform':'rotate(-30deg) translate(0%,-60%) scale(0)'});        
    }

}); 


(function bloc(){
    
    if (matchMedia("screen and (orientation:landscape)").matches) {
        
        console.log('가로모드');

        window.addEventListener('scroll', function(e) {
        
            nowScroll = window.pageYOffset;

            /*bloc1*/
            if(nowScroll > 0 && nowScroll <= 800){multiStyle(bloc_01,{'transform':'rotate(0deg) translate(0px)'});
            }else if(nowScroll > 800 && nowScroll <= 900){multiStyle(bloc_01,{'transform':'rotate(0deg) translate(-10%,-5%)'});
            }else if(nowScroll > 900 && nowScroll <= 1000){multiStyle(bloc_01,{'transform':'rotate(-10deg) translate(-20%,-5%)'});
            }else if(nowScroll > 1000 && nowScroll <= 1100){multiStyle(bloc_01,{'transform':'rotate(-20deg) translate(-30%,-5%)'});
            }else if(nowScroll > 1100 && nowScroll <= 1200){multiStyle(bloc_01,{'transform':'rotate(-30deg) translate(-40%,-5%)'});
            }else if(nowScroll > 1200 && nowScroll <= 1300){multiStyle(bloc_01,{'transform':'rotate(-30deg) translate(-50%,-5%)'});
            }else if(nowScroll > 1300 && nowScroll <= 1400){multiStyle(bloc_01,{'transform':'rotate(-30deg) translate(-60%,-5%)'});
            }else if(nowScroll > 1400 && nowScroll <= 4100){multiStyle(bloc_01,{'transform':'rotate(-30deg) translate(-70%,-5%)'});
            }else if(nowScroll > 4100 && nowScroll <= 4500){multiStyle(bloc_01,{'transform':'rotate(-30deg) translate(-68%,-5%)'});
            }

            /*bloc2*/
            if(nowScroll > 0 && nowScroll <= 1300){
                multiStyle(bloc_02,{'visibility':'hidden'});
//                multiStyle(bloc_02,'visibility','hidden');
            }else if(nowScroll > 0 && nowScroll <= 1300){multiStyle(bloc_02,{'visibility':'hidden', 'transform':'rotate(-30deg) translate(-70%,0%)'});
            }else if(nowScroll > 1300 && nowScroll <= 1400){multiStyle(bloc_02,{'visibility' : 'visible' ,'transform' : 'rotate(-30deg) translate(-70%,0%)'});
            }else if(nowScroll > 1400 && nowScroll <= 1500){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-30deg) translate(-70%,10%)'});
            }else if(nowScroll > 1500 && nowScroll <= 1600){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-30deg) translate(-70%,20%)'});
            }else if(nowScroll > 1600 && nowScroll <= 1700){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-30deg) translate(-70%,30%)'});
            }else if(nowScroll > 1700 && nowScroll <= 1800){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-30deg) translate(-70%,40%)'});
            }else if(nowScroll > 1800 && nowScroll <= 1900){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-30deg) translate(-70%,50%)'});
            }else if(nowScroll > 1900 && nowScroll <= 2000){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-30deg) translate(-70%,60%)'});
            }else if(nowScroll > 2000 && nowScroll <= 4500){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-30deg) translate(-70%,97%)'});
            }

            /*bloc3*/
            if(nowScroll > 0 && nowScroll <= 2000){
                multiStyle(bloc_03,{'visibility':'hidden'});
            }else if(nowScroll > 2000 && nowScroll <= 2100){multiStyle(bloc_03,{'visibility':'hidden','transform':'rotate(-30deg) translate(-70%,97%)'});
            }else if(nowScroll > 2100 && nowScroll <= 2200){multiStyle(bloc_03,{'visibility':'visible','transform':'rotate(-30deg) translate(-70%,97%)'});
            }else if(nowScroll > 2200 && nowScroll <= 2300){multiStyle(bloc_03,{'visibility':'visible','transform':'rotate(-30deg) translate(-60%,97%)'});
            }else if(nowScroll > 2300 && nowScroll <= 2400){multiStyle(bloc_03,{'visibility':'visible','transform':'rotate(-30deg) translate(-50%,97%)'});
            }else if(nowScroll > 2400 && nowScroll <= 2500){multiStyle(bloc_03,{'visibility':'visible','transform':'rotate(-30deg) translate(-40%,97%)'});
            }else if(nowScroll > 2500 && nowScroll <= 4500){multiStyle(bloc_03,{'visibility':'visible','transform':'rotate(-30deg) translate(30%,97%)'});
            } 

            /*bloc4*/
            if(nowScroll > 0 && nowScroll <= 4100){multiStyle(bloc_04,{'visibility':'visible','transform':'rotate(-30deg) translate(31%,-5%)','background-color':'#790d46'});
            }else if(nowScroll > 4100 && nowScroll <= 4500){multiStyle(bloc_04,{'visibility':'visible','transform':'rotate(-30deg) translate(31%,-5%)','background-color':'#ffffff'});
            }

        });


    } else if (matchMedia("screen and (orientation:portrait)").matches) {

        console.log('세로모드');
            

        window.addEventListener('scroll', function(e) {

            nowScroll = window.pageYOffset;
//            console.log(nowScroll);
            /*bloc1*/
            if(nowScroll > 0 && nowScroll <= 800){multiStyle(bloc_01,{'transform':'rotate(0deg) translate(0px)'});
            }else if(nowScroll > 800 && nowScroll <= 900){multiStyle(bloc_01,{'transform':'rotate(0deg) translate(-20%,-5%)'});
            }else if(nowScroll > 900 && nowScroll <= 1000){multiStyle(bloc_01,{'transform':'rotate(-10deg) translate(-40%,-10%)'});
            }else if(nowScroll > 1000 && nowScroll <= 1100){multiStyle(bloc_01,{'transform':'rotate(-20deg) translate(-60%,-15%)'});
            }else if(nowScroll > 1100 && nowScroll <= 1200){multiStyle(bloc_01,{'transform':'rotate(-30deg) translate(-80%,-20%)'});
            }else if(nowScroll > 1200 && nowScroll <= 1300){multiStyle(bloc_01,{'transform':'rotate(-35deg) translate(-90%,-25%)'});
            }else if(nowScroll > 1300 && nowScroll <= 1400){multiStyle(bloc_01,{'transform':'rotate(-35deg) translate(-100%,-30%)'});
            }else if(nowScroll > 1400 && nowScroll <= 4100){multiStyle(bloc_01,{'transform':'rotate(-35deg) translate(-110%,-35%)'});
            }else if(nowScroll > 4100 && nowScroll <= 4500){multiStyle(bloc_01,{'transform':'rotate(-35deg) translate(-110%,-35%)'});
            }

            /*bloc2*/
            if(nowScroll > 0 && nowScroll <= 1400){multiStyle(bloc_02,{'visibility':'hidden','transform':'rotate(-35deg) translate(-130%,-25%)'});
            }else if(nowScroll > 1400 && nowScroll <= 1500){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-35deg) translate(-130%,-20%)'});
            }else if(nowScroll > 1500 && nowScroll <= 1600){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-35deg) translate(-130%,-15%)'});
            }else if(nowScroll > 1600 && nowScroll <= 1700){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-35deg) translate(-130%,-10%)'});
            }else if(nowScroll > 1700 && nowScroll <= 1800){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-35deg) translate(-130%,-5%)'});
            }else if(nowScroll > 1800 && nowScroll <= 1900){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-35deg) translate(-130%,0%)'});
            }else if(nowScroll > 1900 && nowScroll <= 2000){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-35deg) translate(-130%,5%)'});
            }else if(nowScroll > 2000 && nowScroll <= 4500){multiStyle(bloc_02,{'visibility':'visible','transform':'rotate(-35deg) translate(-130%,67%)'});
            }

            /*bloc3*/
            if(nowScroll > 0 && nowScroll <= 2000){
                multiStyle(bloc_03,{'visibility' : 'hidden'});
            }else if(nowScroll > 2000 && nowScroll <= 2100){multiStyle(bloc_03,{'visibility' : 'hidden', 'transform' : 'rotate(-35deg) translate(-120%,67%)'});
            }else if(nowScroll > 2100 && nowScroll <= 2200){multiStyle(bloc_03,{'visibility' : 'visible', 'transform' : 'rotate(-35deg) translate(-110%,67%)'});
            }else if(nowScroll > 2200 && nowScroll <= 2300){multiStyle(bloc_03,{'visibility' : 'visible', 'transform' : 'rotate(-35deg) translate(-100%,67%)'});
            }else if(nowScroll > 2300 && nowScroll <= 2400){multiStyle(bloc_03,{'visibility' : 'visible', 'transform' : 'rotate(-35deg) translate(-90%,67%)'});
            }else if(nowScroll > 2400 && nowScroll <= 2500){multiStyle(bloc_03,{'visibility' : 'visible', 'transform' : 'rotate(-35deg) translate(-80%,67%)'});
            }else if(nowScroll > 2500 && nowScroll <= 4500){multiStyle(bloc_03,{'visibility' : 'visible', 'transform' : 'rotate(-35deg) translate(-26%,67%)'});
            } 

            /*bloc4*/
            if(nowScroll > 0 && nowScroll <= 4100){multiStyle(bloc_04,{'visibility' : 'visible', 'transform' : 'rotate(-35deg) translate(-6%,-35%)','backgroundColor' : '#790d46'});
            }else if(nowScroll > 4100 && nowScroll <= 4500){multiStyle(bloc_04,{'visibility' : 'visible', 'transform' : 'rotate(-35deg) translate(-6%,-35%)','backgroundColor' : '#fff'});
            }
        });

    }
})();

(function skill (){
    
    if(matchMedia("screen and (max-width : 479px)").matches){
        
        console.log('너비가 479보다 작으면 실행');
        
    }else{
        
        window.addEventListener('scroll', function(e) {
            
            nowScroll = window.pageYOffset;

            /*skill1*/
            if(nowScroll > 0 && nowScroll <= 5000){multiStyle(skill_01,{'opacity' : '0','left' : '-100%'});
            }else if(nowScroll > 5000 && nowScroll <= 5500){multiStyle(skill_01,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 5500 && nowScroll <= 6000){multiStyle(skill_01,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 6000 && nowScroll <= 7260){multiStyle(skill_01,{'opacity' : '0','left' : '-100%'});
            }

            /*skill2*/
            if(nowScroll > 0 && nowScroll <= 5000){multiStyle(skill_02,{'opacity' : '0','right' : '-100%'});
            }else if(nowScroll > 5000 && nowScroll <= 5500){multiStyle(skill_02,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 5500 && nowScroll <= 6000){multiStyle(skill_02,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 6000 && nowScroll <= 7260){multiStyle(skill_02,{'opacity' : '0','right' : '-100%'});
            }

            /*skill3*/
            if(nowScroll > 0 && nowScroll <= 5300){multiStyle(skill_03,{'opacity' : '0','left' : '-100%'});
            }else if(nowScroll > 5300 && nowScroll <= 5800){multiStyle(skill_03,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 5800 && nowScroll <= 6300){multiStyle(skill_03,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 6300 && nowScroll <= 7260){multiStyle(skill_03,{'opacity' : '0','left' : '-100%'});
            }


            /*skill4*/
            if(nowScroll > 0 && nowScroll <= 5300){multiStyle(skill_04,{'opacity' : '0','right' : '-100%'});
            }else if(nowScroll > 5300 && nowScroll <= 5800){multiStyle(skill_04,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 5800 && nowScroll <= 6300){multiStyle(skill_04,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 6300 && nowScroll <= 7260){multiStyle(skill_04,{'opacity' : '0','right' : '-100%'});
            }

            /*skill5*/
            if(nowScroll > 0 && nowScroll <= 5600){multiStyle(skill_05,{'opacity' : '0','left' : '-100%'});
            }else if(nowScroll > 5600 && nowScroll <= 6100){multiStyle(skill_05,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 6100 && nowScroll <= 6600){multiStyle(skill_05,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 6600 && nowScroll <= 7260){multiStyle(skill_05,{'opacity' : '0','left' : '-100%'});
            }


            /*skill6*/
            if(nowScroll > 0 && nowScroll <= 5600){multiStyle(skill_06,{'opacity' : '0','right' : '-100%'});
            }else if(nowScroll > 5600 && nowScroll <= 6100){multiStyle(skill_06,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 6100 && nowScroll <= 6600){multiStyle(skill_06,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 6600 && nowScroll <= 7260){multiStyle(skill_06,{'opacity' : '0','right' : '-100%'});
            }

            /*skill7*/
            if(nowScroll > 0 && nowScroll <= 5900){multiStyle(skill_07,{'opacity' : '0','left' : '-100%'});
            }else if(nowScroll > 5900 && nowScroll <= 6400){multiStyle(skill_07,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 6400 && nowScroll <= 6900){multiStyle(skill_07,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 6900 && nowScroll <= 7260){multiStyle(skill_07,{'opacity' : '0','left' : '-100%'});
            }


            /*skill8*/
            if(nowScroll > 0 && nowScroll <= 5900){multiStyle(skill_08,{'opacity' : '0','right' : '-100%'});
            }else if(nowScroll > 5900 && nowScroll <= 6400){multiStyle(skill_08,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 6400 && nowScroll <= 6900){multiStyle(skill_08,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 6900 && nowScroll <= 7260){multiStyle(skill_08,{'opacity' : '0','right' : '-100%'});
            }

            /*skill9*/
            if(nowScroll > 0 && nowScroll <= 6200){multiStyle(skill_09,{'opacity' : '0','left' : '-100%'});
            }else if(nowScroll > 6200 && nowScroll <= 6700){multiStyle(skill_09,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 6700 && nowScroll <= 7200){multiStyle(skill_09,{'opacity' : '1','left' : '0%'});
            }else if(nowScroll > 7200 && nowScroll <= 7260){multiStyle(skill_09,{'opacity' : '0','left' : '-100%'});
            }


            /*skill10*/
            if(nowScroll > 0 && nowScroll <= 6200){multiStyle(skill_10,{'opacity' : '0','right' : '-100%'});
            }else if(nowScroll > 6200 && nowScroll <= 6700){multiStyle(skill_10,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 6700 && nowScroll <= 7200){multiStyle(skill_10,{'opacity' : '1','right' : '0%'});
            }else if(nowScroll > 7200 && nowScroll <= 7260){multiStyle(skill_10,{'opacity' : '0','right' : '-100%'});
            }
        });
        
    }
    
})();

});

function multiStyle(ele, propertyObject){
    for (var property in propertyObject) {
        ele.style[property] = propertyObject[property];
    }
}
function querySelector(name){
    return document['querySelector'](name);
}

//scrollTop
smoothScrollTo = (function () {
  var timer, start, factor;

  return function (target, duration) {
    var offset = window.pageYOffset,
        delta  = target - window.pageYOffset; // Y-offset difference
    duration = duration || 1000;              // default 1 sec animation
    start = Date.now();                       // get start time
    factor = 0;

    if( timer ) {
        
      clearInterval(timer); // stop any running animation
    }

    function step() {
      var y;
      factor = (Date.now() - start) / duration; // get interpolation factor
      if( factor >= 1 ) {
        clearInterval(timer); // stop animation
        factor = 1;           // clip to max 1.0
      } 
      y = factor * delta + offset;
      window.scrollBy(0, y - window.pageYOffset);
    }

    timer = setInterval(step, 10);
    return timer; // return the interval timer, so you can clear it elsewhere
  };
}());