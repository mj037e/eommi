<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, user-scalable=no">
<title>eommi</title>
<link href="./template/css/swiper.css" rel="stylesheet" type="text/css">
<link href="./template/css/common.css" rel="stylesheet" type="text/css">
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script type="text/javascript" src="./template/js/common.js" charset="utf-8"></script>
    <script type="text/javascript" src="./template/js/swiper.js" charset="utf-8"></script>
<!--<script type="text/javascript" src="./template/js/<?=$_GET['mode']?>.js"></script>-->
<!--[if lt IE 9]>
    <script src="./template/js/html5shiv.js"></script>
<![endif]-->
</head>
<body>
<div class="header <?php if(!isset($_GET['mode'])){echo "main";}?>">
    <!--<div class="wrap_inner">-->
<div class="logo_wrap">
    <div class="logo" ><a href="index.php" class="logo_a">daelim</a></div>
</div>
    <!--</div>-->
    <div class="header_top">
        <div class="wrap_inner">
            <div class="gnb">
                <ul class="depth1">
                    <li>
                        <a href="/portfolio/html/index.php?mode=business&con=all">사업분야</a>
                        <ul class="depth2">
                            <li><a href="/portfolio/html/index.php?mode=business&con=all">전체</a></li>
                            <li><a href="/portfolio/html/index.php?mode=business&con=manu01">석유화학</a></li>
                            <li><a href="/portfolio/html/index.php?mode=business&con=manu02">물류·해운</a></li>
                            <li><a href="/portfolio/html/index.php?mode=business&con=manu03">건설정보화</a></li>
                            <li><a href="/portfolio/html/index.php?mode=business&con=manu04">IT서비스</a></li>
                            <li><a href="/portfolio/html/index.php?mode=business&con=manu05">개발사업</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/portfolio/html/index.php?mode=credit_ratingdo&con=manu01">재무정보</a>
                        <ul class="depth2">
                            <li><a href="/portfolio/html/index.php?mode=credit_ratingdo&con=manu01">재무상태표</a></li>
                            <li><a href="/portfolio/html/index.php?mode=credit_ratingdo&con=manu02">손익계산</a></li>
                            <li><a href="/portfolio/html/index.php?mode=credit_ratingdo&con=manu03">신용평가</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/portfolio/html/index.php?mode=list_page&con=menu01">자료실</a>
                        <ul class="depth2">
                            <li><a href="/portfolio/html/index.php?mode=list_page&con=menu01">자료실</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/portfolio/html/index.php?mode=contact&con=c_01&01=c_01_01">고객센터</a>
                        <ul class="depth2">
                            <li><a href="/portfolio/html/index.php?mode=contact&con=inquiry">일반문의</a></li>
                            <li><a href="/portfolio/html/index.php?mode=contact&con=c_01&01=c_01_01">전자시스템</a></li>
                        </ul>
                    </li>
                </ul>
                <a href="/portfolio/html/index.php?mode=sitemap" class="sitemap header_btn d1"><span class="blind">사이트맵</span></a>
                <a href="/portfolio/html/index.php?mode=search" class="search header_btn d1"><span class="blind">통합검색</span></a>
            </div>
        </div>
    </div>
    <div class="wrap_nav">
        <div class="wrap_inner">
            <div class="nav">
                <p class="home"><span class="blind">home</span></p>
            </div>
            <div class="depth">
                <p class="select"></p>
                <div class="d_list"></div>
            </div>
            <div class="depth">
                <p class="select"></p>
                <div class="d_list"></div>
            </div>

        </div>
    </div>
<?php if(!isset($_GET['mode'])){ ?>
    <div class="visual">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide"></div>
                <div class="swiper-slide"></div>
                <div class="swiper-slide"></div>
                <div class="swiper-slide"></div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
        <div class="main_img_contens">
            <p class="main_title">about</p>
            <div class="img_desc">
                daelim corporation
            </div>
            <p class="main_tex">대림코퍼레이션은 석유화학 물류해운 건설정보화, it서비스 개발사업을 수행하는 전문기업으로 지속적인 성장과 차별화된 고객가치를 실현하고 있습니다</p>
            <button type="button">view</button>
        </div>
    </div>
<?php } ?>
</div>

<div class="container <?php if(!isset($_GET['mode'])){echo "main";}?>" >
