document.addEventListener('DOMContentLoaded',function(){

    //header
    (function(){

        var swiper = new Swiper('.visual .swiper-container', {
            spaceBetween: 30,
            centeredSlides: true,
            fadeEffect: {
                crossFade: true
            },
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },

        });

        var swiper = new Swiper('.visual_mask .swiper-container', {
            spaceBetween: 30,
            effect: 'fade',
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                    return '<span class="' + className + '">' + (index + 1) + '</span>';
                },
            },
        });

        //header top에서 nav로 목록 넣기
        var top_depth1 = document.querySelector('.header_top .depth1');//depth1
        var top_depth2 = document.querySelectorAll('.header_top .depth2');//depth
        var nav_depth = document.querySelector('.wrap_nav').querySelectorAll('.depth');

        //header menu 쿠키
        (function(){
            var depth_li = child(top_depth1);
            for(var i = 0; i < depth_li.length; i++){
                depth_li[i].index = i; //depth_li에 index값 생성
                depth_li[i].addEventListener('click',function(e){
                    this.classList.add('active'); //depth1의 li>a를 클릭하면 class='active';
                    setCookie('menu',this.index, 1); //쿠키값 넣기
                });
            }
            //쿠키
            var cookie_data = getCookie('menu') ? getCookie('menu') : -1;
            if(!(cookie_data === -1)){
                depth_li[cookie_data].classList.add('active');
                // nav_depth[0].querySelector('.select').innerText = depth_li[cookie_data].innerText;
            }
            //쿠키삭제 - seache, sitemap, logo를 클릭했을때.
            document.querySelector('.sitemap').addEventListener('click',setCookie('menu', '', -1));
            document.querySelector('.search').addEventListener('click',setCookie('menu', '', -1));
            document.querySelector('.logo_a').addEventListener('click',setCookie('menu', '', -1));
            //쿠키삭제 - 브라우저를 종료하거나
            // console.log(window.closed);
            // if(window.closed){
            //     setCookie('menu', '', -1);
            // }

        })();

        //main_header_top 서브메뉴 hover하면 보이기
        (function(){
            top_depth1.addEventListener('mouseenter',function(){
                this.parentElement.parentElement.parentElement.parentElement.classList.add('on');
            });
            top_depth1.addEventListener('mouseleave',function(){
                this.parentElement.parentElement.parentElement.parentElement.classList.remove('on');
            });
        })();

        //nav_depth 내용 출력.
        (function(){
            nav_list(top_depth1 , 0) //depth1 출력
            for(var i of top_depth2){
                nav_list(i , 1); //depth2출력
            }

            function nav_list(obj , n){
                var parents = parent(obj);
                var children = child(obj);
                var menu_a = [];
                var d_list = [];

                if(getParameters('mode') === 'sitemap' || getParameters('mode') === 'search' ){
                    menu_a= child(parents);

                    for(var i = 0; i < child(parents).length; i++){
                        if(menu_a[i].nodeName === 'A'){
                            if(menu_a[i].getAttribute('href').indexOf(getParameters('mode')) >= 0){
                                nav_depth[0].querySelector('.d_list').appendChild(menu_a[i].cloneNode(true));
                                nav_depth[0].querySelector('.d_list').style.display ="none";
                                nav_depth[1].style.display ="none";
                            }

                        }
                    }
                }else{
                    for(var k = 0 ; k < children.length; k++){
                        menu_a = child(children[k])
                        for(var i = 0; i < menu_a.length ; i++){
                            if(menu_a[i].nodeName === 'A'){
                                if(n === 0){ // nav_depth1
                                    nav_depth[n].querySelector('.d_list').appendChild(menu_a[i].cloneNode(true));
                                }
                                if(n === 1 &&menu_a[i].getAttribute('href').indexOf(getParameters('mode')) >= 0){//nav_depth2
                                    nav_depth[n].querySelector('.d_list').appendChild(menu_a[i].cloneNode(true));
                                }
                            }
                        }
                    }
                }

            }
        })();


        //nav list에서 현재 선택된 페이지 표시.
        (function(){
            nav_on(nav_depth[0],'mode');
            nav_on(nav_depth[1],'con');

            function nav_on(ele, get){
                for(var select of ele.getElementsByTagName('a')){
                    if(select.getAttribute('href').indexOf(getParameters(get)) >= 0){
                        select.classList.add('on');
                        ele.querySelector('.select').innerText = select.innerText;
                    }
                };
            }

        })();


        // nav에 depth를 클릭했을떄.
        (function(){
            for(var i = 0; i < nav_depth.length; i++){
                nav_depth[i].addEventListener('click',select_box);
                nav_depth[i].addEventListener('mouseleave',removeActive);
            }
        })();



        //footer
        (function(){
            var f_selectbox = document.querySelector('.f_select');
            f_selectbox.addEventListener('click',select_box);
            f_selectbox.addEventListener('mouseleave',removeActive);

        })();


    })();



//main
window.addEventListener('scroll', throttle(function (e){

    //header
    (function(){
        var header = document.querySelector('.header');
        e.preventDefault();
        if(window.scrollY > 75){
            header.classList.add('fixed');
        }else{
            header.classList.remove('fixed');
        }
    })();

    //main
    (function(){
        console.log();
        if(!(document.querySelector('.portfolio_wrap') === null)){
            var content = document.querySelector('.portfolio_wrap').getElementsByTagName('ul');
            e.preventDefault();
            for( var i = 0; i <= content.length-1; i++ ){
                var num = 700;//'.content_wrap'의 position top값
                var plus = 500;//content의 height값

                if( window.scrollY >= num + (plus * i) ){
                    content[i].classList.toggle('on',true);
                }
            }
        }
    })();



},500));

    
});




function throttle(fn, threshhold, scope) {
    threshhold || (threshhold = 250);
    var last,
        deferTimer;
    return function () {
        var context = scope || this;

        var now = +new Date,
            args = arguments;
        if (last && now < last + threshhold) {
            // hold on to it
            clearTimeout(deferTimer);
            deferTimer = setTimeout(function () {
                last = now;
                fn.apply(context, args);
            }, threshhold);
        } else {
            last = now;
            fn.apply(context, args);
        }
    };
}

function getParameters(paramName) {
    var returnValue; // 리턴값을 위한 변수 선언
    var url = location.href;// 현재 URL 가져오기
    var parameters = (url.slice(url.indexOf('?') + 1, url.length)).split('&');
    for (var i = 0; i < parameters.length; i++) {// 나누어진 값의 비교를 통해 paramName 으로 요청된 데이터의 값만 return
        var varName = parameters[i].split('=')[0];
        if (varName.toUpperCase() == paramName.toUpperCase()) {
            returnValue = parameters[i].split('=')[1];
            return decodeURIComponent(returnValue);
        }
    }
};


function filebox(){
    var target = document.querySelector('.file_box .file');
    var txt = "";
    console.log
    if ('files' in target) {
        if (target.files.length == 0) {
            txt = "Select one or more files.";
        } else {
            txt = target.value.split('/').pop().split('\\').pop();
        }
    }
    document.querySelector('.file_box .file_name').value = txt;
}

// 쿠키 생성
function setCookie(cName, cValue, cDay){
    var expire = new Date();
    expire.setDate(expire.getDate() + cDay);
    cookies = cName + '=' + escape(cValue) + '; path=/ '; // 한글 깨짐을 막기위해 escape(cValue)를 합니다.
    if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
    document.cookie = cookies;
}

// 쿠키 가져오기
function getCookie(cName) {
    cName = cName + '=';
    var cookieData = document.cookie;
    var start = cookieData.indexOf(cName);
    var cValue = '';
    if(start != -1){
        start += cName.length;
        var end = cookieData.indexOf(';', start);
        if(end == -1)end = cookieData.length;
        cValue = cookieData.substring(start, end);
    }
    return unescape(cValue);
}

function select_box(e){
    var me = e.target;
    this.classList.add('active');
    if (me && me.nodeName === 'A') {
        // e.preventDefault();
        this.querySelector('.select').innerText = me.innerText;
        this.classList.remove('active');
    }
}
function removeActive(){
    this.classList.remove('active');
}

function tab_on(ele,att,get){
    for(var tab of ele){
        if(tab.getAttribute(att).indexOf(getParameters(get))>= 0) {
            tab.classList.add('active');
        }else{
            tab.classList.remove('active');
        }
    };
}

function child (ele){
    return ele.children;
};
function parent (ele){
    return ele.parentNode;
};