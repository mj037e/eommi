</div>
<div class="footer <?php if(!isset($_GET['mode'])){echo "main";}?>">
    <div class="inner_wrap">
        <div class="top_btn" style="display: none;">
            <a href="#"><span class="blind">스크롤 맨 위로 올라가기</span></a>
        </div>

        <section class="link">
            <ul class="footer_list">
                <li><a href="#">개인정보처리방침</a></li>
                <li><a href="#">공정거래자율준수</a></li>
                <li><a href="#">영상정보 처리기기 설치 운영방침</a></li>
                <li><a href="#">찾아오시는 길</a></li>
                <li><a href="#" target="_blank">SCMS 바로가기</a></li>
            </ul>
            <a href="#" target="_blank" title="새 창으로 열기" class="wa_mark"><img src="template/images/footer/wa_mark.png" alt="과학기술정보통신부 WEB ACCESSIBILITY 마크(웹 접근성 품질인증 마크)" title="국가 공인 인증기관 : 웹와치"></a>
            <div class="f_select">
                <p class="btn_family_site">계열사 바로가기 <span class="off"></span></p>
                <div class="wrap_family_site">
                    <ul>
                        <li>
                            <p class="title">건설</p>
                            <ul>
                                <li><a href="#" target="_blank">대림산업㈜ 건설사업</a></li>
                                <li><a href="#" target="_blank">㈜삼호</a></li>
                                <li><a href="#" target="_blank">고려개발㈜</a></li>
                            </ul>
                        </li>
                        <li>
                            <p class="title">석유화학</p>
                            <ul>
                                <li><a href="#" target="_blank">대림산업㈜ 석유화학</a></li>
                            </ul>
                        </li>
                        <li>
                            <p class="title">제조/상사</p>
                            <ul>
                                <li><a href="#" target="_blank">대림C&amp;S㈜</a></li>
                                <li><a href="#" target="_blank">대림자동차공업㈜</a></li>
                            </ul>
                        </li>
                        <li>
                            <p class="title">레저</p>
                            <ul>
                                <li><a href="#" target="_blank">오라관광㈜</a></li>
                            </ul>
                        </li>
                        <li>
                            <p class="title">교육/문화</p>
                            <ul>
                                <li><a href="#" target="_blank">학교법인 대림학원</a></li>
                                <li><a href="#" target="_blank">대림문화재단</a></li>
                            </ul>
                        </li>
                        <li>
                            <p class="title"><a href="#" target="_blank">그룹웨어</a></p>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="copyright">
            <p class="address">
                <span>(04513) 서울특별시 중구 세종대로 39 대한상공회의소빌딩 11층 ㈜대림코퍼레이션</span>
                <span>TEL : 02-3708-3000, 3114</span>
                <span>FAX : 02-771-8921</span>
            </p>
            <p class="copy">Copyright(C) 2017 DAELIM CORPORATION., All rights reserved.</p>
        </section>
    </div>
</div>

</div>

</body>
</html>
