<style>
    .tit_cont02{margin-top:25px;font-size:1rem;color:#333;font-weight:bold;}
    .tit_cont02 + .txt_list{margin-top:10px;}
    .txt_list02 li:before{width:4px;height:1px;}

    table.type3 {margin-top:20px;}
    table.type3 .txt_list{margin-top:0;}
    table.type3{border-top:2px solid #333;}
    table.type3 th, table.type3 td{height:45px;padding:10px;border-right:1px solid #eee;border-bottom:1px solid #ddd;font-size:.9rem;color:#666;}

    table.type3 thead tr{text-align:center;}
    table.type3 thead th{color:#333;}
    table.type3 th{background-color:#f8f8f8;color:#333;text-align:center;}

    /*table.type3 td{vertical-align: top;}*/
    table.type3 thead tr th:last-child, table tbody tr td:last-child{border-right:none;}

    /*.type3 .bg_bg{border-top:1px solid #ccc;}*/
    /*.type3 .bg_bg .b{background-color:#ebf2f7;}*/
    /*.type3 .th_style{padding-left:25px;}*/
    /*.type3 td.b{height:45px;padding-right:10px;font-size:.9rem;text-align:left;color:#333;font-weight:bold;}*/


</style>

<h4 class="tit_cont">이용절차</h4>

<figure class="diag_manage">
<img src="" alt="전자결제시스템 이용절차 흐름도">
<figcaption class="fig_cont">
    <h5 class="tit_cont02">STEP 01 (신고 및 등록)</h5>
    <ul class="txt_list">
        <li>우리은행 전국 모든 지점에서 신고 및 등록이 가능합니다.</li>
        <li>거래은행과 전자방식 외상매출채권 결재(B2B 대출) 시스템 약정에 필요한 여신한도거래약정,  인터넷뱅킹 등의 서류(은행 소정 양식)를 작성하시고 업무 절차를 협의하시기 바랍니다.</li>
        <li>거래 지점에 두번째 항과 같이 전자 결제를 신청하시면 신청사실이 당사에 즉시 통보되어 등록이 되며, 신청 이후 발생되는 납품 대금은 전자결제시스템을 통해 지급이 이루어 집니다.</li>
    </ul>
    <h5 class="tit_cont02">STEP 02 (입금 내역 조회)</h5>
    <ul class="txt_list">
        <li>당사가 귀사에 지급할 납품 대금의 자료를 은행에 온라인으로 통보하면, 은행은 익일 우리은행 인터넷 홈페이지에 지급 내역을 게시해 놓습니다. </li>
        <li>우리은행 인터넷 홈페이지의 기업 인터넷뱅킹에 귀사의 ID와 PASSWORD를 입력하여 접속하게 되면 납품대금 내역을 조회하실 수 있습니다</li>
    </ul>
    <h5 class="tit_cont02">STEP 03 (자금결제)</h5>
    <ul class="txt_list">
        <li>선입금(할인)을 받지 않을 경우
            <ul class="txt_list02">
                <li>지급일(만기일)에 귀사의 등록된 우리은행 계좌로 자동 입금됩니다.</li>
            </ul>
        </li>
        <li>선입금(할인)을 신청할 경우
            <ul class="txt_list02">
                <li>입금내역조회 두번쨰항과 같이 우리은행 인터넷 홈페이지에 접속하여 지정 한 절차에 따라 선입금(할인)신청을 하실 수 있습니다. </li>
                <li>종전의 어음할인처럼 전체금액을 할인하지 않아도 되며, 필요한 금액만 분할하여 선입금(할인) 신청을 하실 수 있습니다. </li>
                <li>인터넷에서 선입급(할인)신청 즉시 할인료 공제 후 귀사의 계좌로 입금 됩니다. <br>
                    ※ 부득이 인터넷을 사용할 수 없는 업체는 은행을 직접 방문하여 처리하실 수도 있습니다.
                </li>
            </ul>
        </li>
    </ul>
</figcaption>

</figure>


<h4 class="tit_cont">선입금 할인</h4>

<div class="table_type_1">
<table class="type3">
    <caption class="blind">선입금 할인료 할인 기간에 대한 안내표</caption>
    <colgroup>
        <col style="width:50%;">
        <col style="width:50%;">
    </colgroup>
    <thead>
    <tr>
        <th scope="col">할인기간</th>
        <th scope="col">할인기간</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <ul class="txt_list">
                <li>선입금 요청일로부터 지급일(만기일)까지 남은 기간을 적용</li>
            </ul>
        </td>
        <td>
            <ul class="txt_list">
                <li>근3영업일간의 91일물 CD유통수익율 평균 + 가산금리 (SPREAD)</li>
                <li>가산금리 (SPREAD)는 지급일까지의 남은 기간에 따라 차등 적용됩니다.</li>
                <li>총액한도대출대상 이외의 업체 (대기업)는 가산금리에 +α%가 추가로 가산됩니다.
                    <br>※ 총액한도대출대상업체 : 중소기업기본법상의 중소기업</li>
            </ul>
        </td>
    </tr>
    </tbody>
</table>
</div>

<h4 class="tit_cont">제출서류</h4>
<h4 class="tit_cont">양식 다운로드</h4>
<div class="table_type1">
<table class="type3">
    <caption>여신거래약정서, 전자금융이용신청서, 추가약정서, 채권양도통지서, 지정계좌신고서, 채권양도계약서 양식 다운로드 안내 표</caption>
    <colgroup>
        <col style="width: 12.5%;">
        <col style="width: 37.5%;">
        <col style="width: 12.5%;">
        <col style="width: 37.5%;">
    </colgroup>
    <tbody>
    <tr>
        <th scope="row">여신거래약정서</th>
        <td>우리은행 창구에 있음</td>
        <th scope="row">전자금융이용신청서</th>
        <td>우리은행 창구에 있음</td>
    </tr>
    <tr>
        <th scope="row">추가약정서</th>
        <td>
            <div class="file_down">
                <a href="https://itc.daelimcorp.co.kr/fileDownload.do?folder=etc&amp;filenm=form_LoanAdditionalAgreement_020805.zip" target="_blank">다운로드</a>
            </div>
        </td>
        <th scope="row">채권양도통지서</th>
        <td>
            <div class="file_down">
                <a href="https://itc.daelimcorp.co.kr/fileDownload.do?folder=etc&amp;filenm=form_bondTransferNotice_020805.zip" target="_blank">다운로드</a>
            </div>
        </td>
    </tr>
    <tr>
        <th scope="row">지정계좌신고서</th>
        <td>
            <div class="file_down">
                <a href="https://itc.daelimcorp.co.kr/fileDownload.do?folder=etc&amp;filenm=form_appointedAccountReport.zip" target="_blank">다운로드</a>
            </div>
        </td>
        <th scope="row">채권양도계약서</th>
        <td>
            <div class="file_down">
                <a href="https://itc.daelimcorp.co.kr/fileDownload.do?folder=etc&amp;filenm=form_bondTransferContract_020805.zip" target="_blank">다운로드</a>
            </div>
        </td>
    </tr>
    </tbody>
</table>
</div>
<h4 class="tit_cont">담당자 연락처</h4>
<div class="table_type_1">
<table class="type3">
    <caption>담당자의 부서, 업무, 전화번호 안내 표</caption>
    <colgroup>
        <col style="width:33.3%;">
        <col style="width:33.3%;">
        <col style="width:33.3%;">
    </colgroup>
    <thead>
    <tr>
        <th scope="col">담당부서</th>
        <th scope="col">담당업무</th>
        <th scope="col">전화번호</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>재무팀</td>
        <td>지급관련</td>
        <td>02-3708-3271</td>
    </tr>
    <tr>
        <td>재무팀</td>
        <td>전자결제</td>
        <td>02-3704-8465</td>
    </tr>
    <tr>
        <td>재무팀</td>
        <td>거래처 등록</td>
        <td>02-3708-3271</td>
    </tr>
    </tbody>
</table>
</div>
