<div class="inquiry">
<div class="visual">

    <div class="cover">
        <div class="cover_inner">
            <h2>고객센터</h2>
            <p>궁금하신 사항을 문의해 주세요.</p>
        </div>
    </div>
</div>
<div class="content_wrap form_type01">
    <div class="sub_title">
        <h3>일반문의</h3>
    </div>
    <div class="sub_content">
        <div class="input_box">
            <h4>고객정보</h4>
            <p class="check_txt">
                <span class="check"><span class="blind">필수입렵</span></span>
                체크된 항목은 필수입력항목 입니다.
            </p>
            <table>
                <caption>문의해주시는 고객님의 정보를 입력하는 항목. 고객명, 이메일, 연락처 입력란이 있음.</caption>
                <colgroup>
                    <col style="width:20%">
                    <col style="width:80%">
                </colgroup>
                <tbody>
                <tr>
                    <th>
                        <label for="txt_form01">
                            <span class="check"><span class="blind">필수입렵</span></span>
                            고객
                        </label>
                    </th>
                    <td><input type="text" id="txt_form01" class="input_width01" title="고객명을 입력하세요" maxlength="10"/></td>
                </tr>
                <tr>
                    <th>
                        <label for="txt_form02">
                            <span class="check"><span class="blind">필수입렵</span></span>
                            이메일
                        </label>
                    </th>
                    <td>
                        <input type="text" id="txt_form02" class="input_width01" title="이메일 아이디를 입력하세요" maxlength="20"/>
                        <span>@</span>
                        <input type="text" class="input_width01" title="이메일 뒷 주소를 입력하세요" maxlength="30"/>
                        <div class="select_box">
                            <p href="#" class="select">직접입력</p>
                            <ul>
                                <li><a href="#">직접입력</a></li>
                                <li><a href="#">naver.com</a></li>
                                <li><a href="#">hanmail.net</a></li>
                                <li><a href="#">gmail.com</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="txt_form03_01">
                            연락
                        </label>
                    </th>
                    <td>
                        <input type="text" id="txt_form03_01" class="input_width01" title="연락처 앞번호를 입력하세요" maxlength="3"/>
                        <span> - </span>
                        <input type="text" id="txt_form03_02" class="input_width01" title="연락처 중간번호를 입력하세요" maxlength="4"/>
                        <span> - </span>
                        <input type="text" id="txt_form03_03" class="input_width01" title="연락처 뒷호를 입력하세요" maxlength="4"/>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="input_box">
            <h4>문의사항</h4>
            <table>
                <caption>문의해주시는 고객님의 정보를 입력하는 항목. 고객명, 이메일, 연락처 입력란이 있음.</caption>
                <colgroup>
                    <col style="width:20%">
                    <col style="width:80%">
                </colgroup>
                <tbody>
                <tr>
                    <th>
                        <label for="txt_form01">
                            <span class="check"><span class="blind">필수입렵</span></span>
                            문의유형
                        </label>
                    </th>
                    <td>
                        <div class="radio">

                            <input type="radio" class="radio" id="radioForm1" name="inquiry_type01"  >
                            <label for="radioForm1">채용</label>

                            <input type="radio" class="radio" id="radioForm2" name="inquiry_type01" >
                            <label for="radioForm2">IR(투자)</label>

                            <input type="radio" class="radio" id="radioForm3" name="inquiry_type01" >
                            <label for="radioForm3">사이트 이용</label>

                            <input type="radio" class="radio" id="radioForm4" name="inquiry_type01"  >
                            <label for="radioForm4">기타</label>

                        </div>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="txt_form04">
                            <span class="check"><span class="blind">필수입렵</span></span>
                            제목
                        </label>
                    </th>
                    <td>
                        <input type="text" id="txt_form04" title="문의사항 글의 제목을 입력하세요" maxlength="50"/>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="txt_form05">
                            <span class="check"><span class="blind">필수입렵</span></span>
                            문의내용
                        </label>
                    </th>
                    <td>
                        <textarea id="txt_form05" title="문의하실 내용을 입력하세요" placeholder="문의내용을 입력해주세요. 자세히 적어주시면 더욱 상세한 답변을 전해드릴 수 있습니다." name="cusContent" maxlength="500"></textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label for="txt_form03_01">
                            첨부파일
                        </label>
                    </th>
                    <td>
                        <div class="file_box">
                            <input class="file_name" value="파일선택" disabled="disabled">
                            <label for="ex_filename">파일첨부</label>
                            <input type="file" id="ex_filename" class="file" onchange="filebox()">
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="input_box">
            <h4>개인정보 수집 및 이용 동의</h4>
            <div class="txt_box">

                개인정보수집 및 이용에 대한 안내<br>
                '(주)회사상호' 은 고객님의 문의요청에 정확하고 성실한 답변을 드리기 위해 필요한 최소한의 개인정보를 수집하고 있습니다.<br>
                이에 개인정보의 수집 및 이용에 관하여 아래와 같이 고지하오니 충분히 읽어보신 후 동의하여 주시기 바랍니다.<br><br>

                1. 수집 이용목적 : 본인확인, 고객문의, 민원처리<br><br>

                2. 개인정보 수집항목<br>
                필수항목 : 고객명, 이메일, 문의유형, 제목, 문의내용<br>
                선택항목 : 연락처, 첨부파일<br><br>

                3. 보유 및 이용기간<br>
                - 접수 후 0개월<br><br>

                4. 개인정보수집 및 이용 동의 거부안내<br>
                - 고객님은 본 서비스의 개인정보 수집 및 이용 동의에 거부하실 수 있으며 이 경우 본 서비스를 이용하실 수 없습니다.

            </div>
            <div class="txt_checkbox">
                <span>개인정보의 수집 및 이용에 동의하십니까?(필수)</span>
                <input type="radio" class="radio" id="formY" name="inquiry_type02">
                <label for="formY">동의</label>
                <input type="radio" class="radio" id="formN" name="inquiry_type02">
                <label for="formN">동의하지 않음</label>
            </div>
            <div class="btn_wrap">
                <button type="button" class="btn btn_color01">취소</button>
                <button type="button" class="btn btn_color02">문의</button>
            </div>
        </div>


    </div>



</div>
</div>

<script>
//select_box기능
document.querySelector('.select_box').addEventListener('click',select_box);
document.querySelector('.select_box').addEventListener('mouseleave',removeActive);
</script>