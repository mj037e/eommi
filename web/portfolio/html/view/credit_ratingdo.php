<div class="credit_ratingdo">
    <div class="visual">

    <div class="cover">
        <div class="cover_inner">
            <h2>재무정보</h2>
            <p>재무정보를 정확하고 신속하게 제공해 드립니다.</p>
        </div>
    </div>
    </div>
    <div class="content_wrap">

    <div class="tabs" id="tabs">
        <h3 class="tab" onclick="location.href='index.php?mode=credit_ratingdo&con=manu01'">재무상태표</h3>
        <div class="tabmenu">
            <div class="tab_inner">
                <div class="sub_title">
                    <h3>재무상태</h3>
                </div>
                <div class="sub_content">
                    <span class="t_side_info">(단위:백만원)</span>
                    <div class="table_type_1">

                        <table class="type1">

                            <caption>대림코퍼레이션 요약 대차대조표. 항목,2015, 2014, 2013,2012,2011으로 구분하여 정리됨</caption>

                            <colgroup>

                                <col style="width:15%;">

                                <col>

                                <col>

                                <col>

                                <col>

                                <col>

                                <col>

                            </colgroup>



                            <thead>

                            <tr>

                                <th scope="col">항목</th>
                                <th scope="col">2017</th>

                                <th scope="col">2016</th>

                                <th scope="col">2015</th>

                                <th scope="col">2014</th>

                                <th scope="col">2013</th>

                                <th scope="col">2012</th>

                                <!--th scope="col">2011</th-->

                            </tr>

                            </thead>



                            <tbody>

                            <tr class="bg_bg">

                                <td class="b">자산</td>
                                <td>2,524,750</td>

                                <td>2,410,434</td>

                                <td>2,389,915</td>

                                <td>1,979,101</td>

                                <td>2,112,000</td>

                                <td>2,335,551</td>

                                <!--td>2,006,157</td-->

                            </tr>

                            <tr>

                                <td class="b">1. 유동자산</td>
                                <td>542,744</td>

                                <td>500,658</td>

                                <td>479,688</td>

                                <td>423,658</td>

                                <td>403,789</td>

                                <td>640,785</td>

                                <!--td>432,534</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">현금 및 현금성 자산</td>
                                <td>92,434</td>

                                <td>104,185</td>

                                <td>99,304</td>

                                <td>42,592</td>

                                <td>54,686</td>

                                <td>27,855</td>

                                <!--td>47,579</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">단기금융상품</td>
                                <td>50,000</td>

                                <td></td>

                                <td>700</td>

                                <td></td>

                                <td></td>

                                <td></td>

                                <!--td>1,000</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">매출채권 및 기타채권</td>

                                <td>333,802</td>
                                <td>337,712</td>

                                <td>317,747</td>

                                <td>274,623</td>

                                <td>292,924</td>

                                <td>559,425</td>

                                <!--td>326,596</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">파생상품자산</td>
                                <td>7,051</td>

                                <td>16,531</td>

                                <td>2,481</td>

                                <td>30,776</td>

                                <td>4,838</td>

                                <td>3,651</td>

                                <!--td>199</td-->


                            </tr>

                            <tr>

                                <td class="b th_style">재고자산</td>
                                <td>41,803</td>

                                <td>21,858</td>

                                <td>39,342</td>

                                <td>49,736</td>

                                <td>41,873</td>

                                <td>44,087</td>

                                <!--td>50,453</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">기타유동자산</td>
                                <td>17,644</td>

                                <td>20,370</td>

                                <td>20,114</td>

                                <td>25,930</td>

                                <td>9,357</td>

                                <td>5,766</td>

                                <!--td>6,706</td-->

                            </tr>

                            <tr>

                                <td class="b">2. 비유동자산</td>
                                <td>1,982,006</td>

                                <td>1,909,775</td>

                                <td>1,910,227</td>

                                <td>1,555,443</td>

                                <td>1,708,322</td>

                                <td>1,694,766</td>

                                <!--td>1,573,623</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">장기금융상품</td>
                                <td>10</td>

                                <td>10</td>

                                <td>2,260</td>

                                <td>1,208</td>

                                <td>8</td>

                                <td>8</td>

                                <!--td>8</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">매출채권 및 기타채권</td>

                                <td>4,075</td>
                                <td>5,743</td>

                                <td>7,433</td>

                                <td>997</td>

                                <td>973</td>

                                <td>983</td>

                                <!--td>950</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">매도가능금융자산</td>
                                <td>6,103</td>

                                <td>7,518</td>

                                <td>6,418</td>

                                <td>924</td>

                                <td>1,424</td>

                                <td>1,665</td>

                                <!--td>1,763</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">관계기업투자주식</td>
                                <td>1,078,022</td>

                                <td>975,842</td>

                                <td>928,034</td>

                                <td>935,830</td>

                                <td>1,000,903</td>

                                <td>1,011,293</td>

                                <!--td>940,996</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">유형자산</td>
                                <td>439,730</td>

                                <td>458,634</td>

                                <td>503,946</td>

                                <td>530,435</td>

                                <td>616,104</td>

                                <td>585,416</td>

                                <!--td>536,551</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">투자부동산</td>
                                <td>208,646</td>

                                <td>210,749</td>

                                <td>205,563</td>

                                <td></td>

                                <td></td>

                                <td></td>

                                <!--td></td-->

                            </tr>

                            <tr>

                                <td class="b th_style">무형자산</td>
                                <td>237,973</td>

                                <td>243,083</td>

                                <td>247,368</td>

                                <td>76,795</td>

                                <td>86,884</td>

                                <td>87,856</td>

                                <!--td>87,831</td-->

                            </tr>

                            <tr>

                                <td class="b th_style">기타유동자산</td>
                                <td>7,447</td>

                                <td>8,196</td>

                                <td>9,205</td>

                                <td>9,316</td>

                                <td>12,115</td>

                                <td>8,517</td>

                                <!--td>5,500</td-->

                            </tr>

                            <tr class="bg_bg">

                                <td class="b">부채</td>
                                <td>1,210,916</td>

                                <td>1,232,608</td>

                                <td>1,269,928</td>

                                <td>1,067,607</td>

                                <td>1,051,932</td>

                                <td>1,037,171</td>

                                <!--td>1,071,582</td-->

                            </tr>

                            <tr>

                                <td class="b">1. 유동부채</td>
                                <td>566,680</td>

                                <td>605,962</td>

                                <td>687,800</td>

                                <td>314,959</td>

                                <td>476,384</td>

                                <td>737,046</td>

                                <!--td>395,101</td-->

                            </tr>

                            <tr>

                                <td class="b">2. 비유동부채</td>
                                <td>644,235</td>

                                <td>626,645</td>

                                <td>582,128</td>

                                <td>752,648</td>

                                <td>575,548</td>

                                <td>570,126</td>

                                <!--td>676,481</td-->

                            </tr>

                            <tr class="bg_bg">

                                <td class="b">자본</td>
                                <td>1,313,835</td>

                                <td>1,177,826</td>

                                <td>1,119,987</td>

                                <td>911,493</td>

                                <td>1,060,068</td>

                                <td>1,028,380</td>

                                <!--td>934,575</td-->

                            </tr>

                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
        <h3 class="tab" onclick="location.href='index.php?mode=credit_ratingdo&con=manu02'">손익계산</h3>
        <div class="tabmenu">
            <div class="tab_inner">
                <div class="sub_title">
                    <h3>손익계산서</h3>
                </div>
                <div class="sub_content">

                    <span class="t_side_info">(단위:백만원)</span>

                    <div class="table_type_1">

                        <table class="type1">

                            <caption>대림코퍼레이션 요약 손익계산서. 항목,2015, 2014, 2013,2012,2011으로 구분하여 정리됨</caption>

                            <colgroup>

                                <col style="width:15%;">

                                <col>

                                <col>

                                <col>

                                <col>

                                <col>

                                <col>

                            </colgroup>



                            <thead>

                            <tr>

                                <th scope="col">항목</th>
                                <th scope="col">2017</th>

                                <th scope="col">2016</th>

                                <th scope="col">2015</th>

                                <th scope="col">2014</th>

                                <th scope="col">2013</th>

                                <th scope="col">2012</th>


                            </tr>

                            </thead>



                            <tbody>

                            <tr class="bg_bg">

                                <td class="b th_style2">매출액</td>
                                <td>3,202,289</td>

                                <td>2,628,219</td>

                                <td>2,589,440</td>

                                <td>3,562,922</td>

                                <td>4,136,450</td>

                                <td>4,175,450</td>


                            </tr>

                            <tr>

                                <td class="b th_style2">매출원가</td>
                                <td>2,971,510</td>

                                <td>2,424,770</td>

                                <td>2,434,164</td>

                                <td>3,454,578</td>

                                <td>3,977,232</td>

                                <td>4,040,244</td>



                            </tr>

                            <tr class="bg_bg">

                                <td class="b th_style2">매출총이익</td>
                                <td>230,779</td>

                                <td>203,449</td>

                                <td>155,276</td>

                                <td>108,343</td>

                                <td>159,218</td>

                                <td>135,559</td>

                            </tr>

                            <tr>

                                <td class="b th_style2">판매비 및 관리비</td>
                                <td>94,933</td>

                                <td>90,997</td>

                                <td>74,957</td>

                                <td>68,045</td>

                                <td>69,754</td>

                                <td>72,032</td>


                            </tr>

                            <tr class="bg_bg">

                                <td class="b th_style2">영업이익</td>
                                <td>135,847</td>

                                <td>112,451</td>

                                <td>80,319</td>

                                <td>40,298</td>

                                <td>89,464</td>

                                <td>63,527</td>

                            </tr>

                            <tr>

                                <td class="b th_style2">법인세차감전순이익</td>
                                <td>207,228</td>

                                <td>124,295</td>

                                <td>2,255</td>

                                <td>-139,552</td>

                                <td>64,433</td>

                                <td>158,693</td>


                            </tr>

                            <tr class="bg_bg">

                                <td class="b th_style2">법인세비용</td>
                                <td>50,398</td>

                                <td>52,488</td>

                                <td>-1,582</td>

                                <td>-24,999</td>

                                <td>17,463</td>

                                <td>37,794</td>


                            </tr>

                            <tr>

                                <td class="b th_style2">당기순이익</td>
                                <td>156,829</td>

                                <td>71,807</td>

                                <td>3,837</td>

                                <td>-114,553</td>

                                <td>46,970</td>

                                <td>120,900</td>


                            </tr>

                            </tbody>

                        </table>

                    </div>

                </div>
            </div>
        </div>
        <h3 class="tab" onclick="location.href='index.php?mode=credit_ratingdo&con=manu03'">신용평가</h3>
        <div class="tabmenu">
            <div class="tab_inner">
                <div class="sub_title">
                    <h3>신용평가</h3>
                </div>

                <div class="sub_content">

                    <h4 class="table_title">회사채 신용평가</h4>
                    <div class="table_type_1">
                        <table class="type1">

                            <caption>회사채 신용평가 정보. 항목, 등급, 평가 기준일로 구분함.</caption>

                            <colgroup>

                                <col style="width:33.333%">

                                <col style="width:33.333%">

                                <col style="width:33.333%">

                            </colgroup>

                            <thead>

                            <tr>

                                <th scope="col">항목</th>

                                <th scope="col">등급</th>

                                <th scope="col">평가기준일</th>

                            </tr>

                            </thead>



                            <tbody>

                            <tr>

                                <td class="b th_style3">NICE 신용평가</td>

                                <td>A</td>

                                <td>2017.06.28</td>

                            </tr>

                            <tr>

                                <td class="b th_style3">한국 신용평가</td>

                                <td>A</td>

                                <td>2017.06.15</td>

                            </tr>

                            <tr>

                                <td class="b th_style3">한국 기업평가</td>

                                <td>A</td>

                                <td>2017.06.15</td>

                            </tr>

                            </tbody>

                        </table>
                    </div>

                    <h4 class="table_title">기업어음 신용평가</h4>
                    <div class="table_type_1">

                        <table class="type1">

                            <caption>기업어음 신용평가 정보. 항목, 등급, 평가 기준일로 구분함.</caption>

                            <colgroup>

                                <col style="width:33.333%">

                                <col style="width:33.333%">

                                <col style="width:33.333%">

                            </colgroup>

                            <thead>

                            <tr>

                                <th scope="col">항목</th>

                                <th scope="col">등급</th>

                                <th scope="col">평가기준일</th>

                            </tr>

                            </thead>



                            <tbody>

                            <tr>

                                <td class="b th_style3">NICE 신용평가</td>

                                <td>A2</td>

                                <td>2017.06.28</td>

                            </tr>

                            <tr>

                                <td class="b th_style3">한국 신용평가</td>

                                <td>A2</td>

                                <td>2017.06.15</td>

                            </tr>

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>
        </div>

    </div>

    </div>
</div>

<script>
tab_on(document.querySelectorAll('.tab'),'onClick','con');//tab기능
</script>