<div class="sitemap">
    <div class="content_wrap">
        <div class="sub_title">
            <h2>사이트맵</h2>
        </div>
        <div class="wrap_sitemap">
            <article class="cell_sitemap">
                <h4 class="stit_sitemap">사업분야</h4>
                <ul class="list_sitemap">
                    <!-- <li><a href="/ko/pc/business-areas/business-overview.do">사업개요</a></li> -->
                    <li>
                        <a href="/ko/pc/business-areas/business-area.do?menu1">석유화학</a>
                        <ul class="list_2dpt_sitemap">
                            <li>
                                <a href="/ko/pc/business-areas/polymer/polyethylene-hdpe.do">폴리머</a>
                            </li>
                            <li>
                                <a href="/ko/pc/business-areas/chemistry/petroleum-products.do">화학</a>
                            </li>
                            <li>
                                <a href="/ko/pc/business-areas/polyimide/about-plavis.do">폴리이미드</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="list_sitemap">
                    <li>
                        <a href="/ko/pc/business-areas/business-area.do?menu2">물류·해운</a>
                        <ul class="list_2dpt_sitemap">
                            <li>
                                <a href="/ko/pc/business-areas/distribution/distribution1.do">물류</a>
                            </li>
                            <li>
                                <a href="/ko/pc/business-areas/vessel/vessel1.do">선박</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="list_sitemap">
                    <li>
                        <a href="/ko/pc/business-areas/business-area.do?menu3">건설정보화</a>
                        <ul class="list_2dpt_sitemap">
                            <li>
                                <a href="/ko/pc/business-areas/construction-infra/construction-infra1.do">건설인프라</a>
                            </li>
                            <li>
                                <a href="/ko/pc/business-areas/architectural-solution/ibs.do">건축솔루션</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="/ko/pc/business-areas/business-area.do?menu4">IT서비스</a>
                        <ul class="list_2dpt_sitemap">
                            <li><a href="/ko/pc/business-areas/it-service/it-service1.do">ITO &amp; BPO</a></li>
                            <li><a href="/ko/pc/business-areas/it-service/it-service2.do">IT 솔루션 및 플랫폼</a></li>
                            <li><a href="/ko/pc/business-areas/it-service/it-service3.do">커뮤니케이션 서비스</a></li>
                        </ul>
                    </li>
                    <li><a href="/ko/pc/business-areas/business-area.do?menu5">개발사업</a></li>
                </ul>
            </article>
            <article class="cell_sitemap">
                <h4 class="stit_sitemap">재무정보</h4>
                <ul class="list_sitemap">
                    <li><a href="/ko/pc/ir/b-s.do">재무제표</a></li>
                    <li><a href="/ko/pc/ir/official-notice-info.do">공시정보</a></li>
                    <li><a href="/ko/pc/ir/justice-deal1.do">공정거래자율준수</a></li>
                </ul>
            </article>
            <article class="cell_sitemap">
                <h4 class="stit_sitemap">자료실</h4>
                <ul class="list_sitemap">
                    <li><a href="/ko/pc/pr-room/news.do">자료실</a></li>
                </ul>
            </article>
            <article class="cell_sitemap">
                <h4 class="stit_sitemap">고객센터</h4>
                <ul class="list_sitemap">
                    <li><a href="/ko/pc/contact-us/general-inquiry.do">일반 문의</a></li>
                    <li><a href="/ko/pc/contact-us/elt-settle01.do">전자결제시스템</a></li>
                </ul>
            </article>
            <article class="cell_sitemap">
                <h4 class="stit_sitemap">통합검색</h4>
                <ul class="list_sitemap">
                    <li><a href="/ko/pc/contact-us/general-inquiry.do">통합검색</a></li>
                </ul>
            </article>
        </div>


    </div>
</div>