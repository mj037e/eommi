<div class="business">
    <div class="visual">

        <div class="cover">
            <div class="cover_inner">
                <h2>사업분야</h2>
                <p>대림코퍼레이션 구성원들은 목표 의식을 가지고 끈기 있는 도전과 상호 간의 협력을 실천하여 <br/>  개인과 조직의 공동 성장을 추구합니다.</p>
            </div>
        </div>
    </div>
    <div class="content_wrap">

            <div class="tabs" id="tabs">
                <h3 class="tab" onclick="location.href='index.php?mode=business&con=all'">전체</h3>
                <div class="tabmenu">
                    <div class="tab_inner">
                        <div class="img_info">
                            <div class="img_wrap">
                                <img src="template/images/business/b_list_img0.png" alt=""/>
                            </div>
                            <div class="info_wrap">
                                <div class="cover">
                                    <div class="cover_inner">
                                        <h3>대림코퍼레이션 사업분야</h3>
                                        <p>
                                            대림코퍼레이션 구성원들은 목표 의식을 가지고 끈기 있는 도전과 상호 간의 협력을 실천하여 개인과 조직의 공동 성장을 추구합니다.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_business_wrap">
                            <ul class="polymer">
                                <li>폴리머</li>
                                <li>폴리에틸렌<button type="button">바로가기</button></li>
                                <li>폴리프로필<button type="button">바로가기</button></li>
                                <li>폴리부텐<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="chemistry">
                                <li>화학</li>
                                <li>석유제품<button type="button">바로가기</button></li>
                                <li>모노머<button type="button">바로가기</button></li>
                                <li>케미칼<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="polyimide">
                                <li>폴리이미드</li>
                                <li>PLAVIS소개<button type="button">바로가기</button></li>
                                <li>기술 개요<button type="button">바로가기</button></li>
                                <li>사용 분<button type="button">바로가기</button></li>
                                <li>국내·해외 지사<button type="button">바로가기</button></li>
                                <li>인증서<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="distribution">
                                <li>물류</li>
                                <li>복합물류<button type="button">바로가기</button></li>
                                <li>특수물<button type="button">바로가기</button></li>
                                <li>ISO 탱크 컨테이<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="vessel">
                                <li>선박</li>
                                <li>가스<button type="button">바로가기</button></li>
                                <li>케미칼선<button type="button">바로가기</button></li>
                                <li>CPP선<button type="button">바로가기</button></li>
                                <li>벌크선<button type="button">바로가기</button></li>
                                <li>선대정보<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="build">
                                <li>건설인프라</li>
                                <li>토목기전<button type="button">바로가기</button></li>
                                <li>건축기전<button type="button">바로가기</button></li>
                                <li>신재생 에너<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="architecture">
                                <li>건축솔루션</li>
                                <li>IBS<button type="button">바로가기</button></li>
                                <li>건축자재<button type="button">바로가기</button></li>
                                <li>커뮤니티<button type="button">바로가기</button></li>
                                <li>부동산 종합관리<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="it_service">
                                <li>IT서비스</li>
                                <li>ITO & BPO<button type="button">바로가기</button></li>
                                <li>IT 솔루션 및 플랫폼<button type="button">바로가기</button></li>
                                <li>커뮤니케잉션 서비스<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="development_work">
                                <li>개발사업</li>
                                <li>개발사업<button type="button">바로가기</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h3 class="tab" onclick="location.href='index.php?mode=business&con=manu01'">석유화학</h3>
                <div class="tabmenu">
                    <div class="tab_inner">
                        <div class="img_info">
                            <div class="img_wrap">
                                <img src="template/images/business/b_list_img1.png" alt=""/>
                            </div>
                            <div class="info_wrap">
                                <div class="cover">
                                    <div class="cover_inner">
                                        <h3>석유화학</h3>
                                        <p>
                                            화학 및 폴리머 제품의 국내외 판매 및 고기능 플라스틱 소재인 폴리이미드
                                            제품을 생산, 판매하는 석유화학 트레이딩 전문 기업으로 고객 가치 및
                                            성과를 창출하고 있습니다
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_business_wrap">
                            <ul class="polymer">
                                <li>폴리머</li>
                                <li>폴리에틸렌<button type="button">바로가기</button></li>
                                <li>폴리프로필<button type="button">바로가기</button></li>
                                <li>폴리부텐<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="chemistry">
                                <li>화학</li>
                                <li>석유제품<button type="button">바로가기</button></li>
                                <li>모노머<button type="button">바로가기</button></li>
                                <li>케미칼<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="polyimide">
                                <li>폴리이미드</li>
                                <li>PLAVIS소개<button type="button">바로가기</button></li>
                                <li>기술 개요<button type="button">바로가기</button></li>
                                <li>사용 분<button type="button">바로가기</button></li>
                                <li>국내·해외 지사<button type="button">바로가기</button></li>
                                <li>인증서<button type="button">바로가기</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h3 class="tab" onclick="location.href='index.php?mode=business&con=manu02'">물류·해운</h3>
                <div class="tabmenu">
                    <div class="tab_inner">
                        <div class="img_info">
                            <div class="img_wrap">
                                <img src="template/images/business/b_list_img2.png" alt=""/>
                            </div>
                            <div class="info_wrap">
                                <div class="cover">
                                    <div class="cover_inner">
                                        <h3>물류·해운</h3>
                                        <p>
                                            세계를 연결하는 글로벌 네트워크를 바탕으로 차별화된
                                            대림코퍼레이션의 물류·해운 서비스는 경쟁력있는 운임과 다양한
                                            운송 방법으로 고객에게 최고의 가치를 제공하고 있습니다.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_business_wrap">
                            <ul class="distribution">
                                <li>물류</li>
                                <li>복합물류<button type="button">바로가기</button></li>
                                <li>특수물<button type="button">바로가기</button></li>
                                <li>ISO 탱크 컨테이<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="vessel">
                                <li>선박</li>
                                <li>가스<button type="button">바로가기</button></li>
                                <li>케미칼선<button type="button">바로가기</button></li>
                                <li>CPP선<button type="button">바로가기</button></li>
                                <li>벌크선<button type="button">바로가기</button></li>
                                <li>선대정보<button type="button">바로가기</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h3 class="tab" onclick="location.href='index.php?mode=business&con=manu03'">건설정보화</h3>
                <div class="tabmenu">
                    <div class="tab_inner">
                        <div class="img_info">
                            <div class="img_wrap">
                                <img src="template/images/business/b_list_img3.png" alt=""/>
                            </div>
                            <div class="info_wrap">
                                <div class="cover">
                                    <div class="cover_inner">
                                        <h3>건설 정보화</h3>
                                        <p>
                                            건물자체의 부가가치 상승과 주택, 터널, 도로, 항만, 철도, 교량 등
                                            건축물에 대한 기획부터 설계, 시공, A/S로 이어지는
                                            Total Service 를 제공합니다.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_business_wrap">
                            <ul class="build">
                                <li>건설인프라</li>
                                <li>토목기전<button type="button">바로가기</button></li>
                                <li>건축기전<button type="button">바로가기</button></li>
                                <li>신재생 에너<button type="button">바로가기</button></li>
                            </ul>
                            <ul class="architecture">
                                <li>건축솔루션</li>
                                <li>IBS<button type="button">바로가기</button></li>
                                <li>건축자재<button type="button">바로가기</button></li>
                                <li>커뮤니티<button type="button">바로가기</button></li>
                                <li>부동산 종합관리<button type="button">바로가기</button></li>
                            </ul>

                        </div>
                    </div>
                </div>
                <h3 class="tab" onclick="location.href='index.php?mode=business&con=manu04'">IT서비스</h3>
                <div class="tabmenu">
                    <div class="tab_inner">
                        <div class="img_info">
                            <div class="img_wrap">
                                <img src="template/images/business/b_list_img4.png" alt=""/>
                            </div>
                            <div class="info_wrap">
                                <div class="cover">
                                    <div class="cover_inner">
                                        <h3>IT서비스</h3>
                                        <p>
                                            정보통신 시스템 통합, 유지보수 및 대외 SI 사업으로 지속적인 성장을
                                            하였으며, 특히 공공부문 정보화 사업에 대한 풍부한 경험과 다양한
                                            아키텍쳐 하에서의 정보시스템 통합 기술력, 표준화된 방법론 및 품질보증
                                            체계절차를 활용하여 고객에게 최적의 IT 서비스를 제공하고 있습니다.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_business_wrap">
                            <ul class="it_service">
                                <li>IT서비스</li>
                                <li>ITO & BPO<button type="button">바로가기</button></li>
                                <li>IT 솔루션 및 플랫폼<button type="button">바로가기</button></li>
                                <li>커뮤니케잉션 서비스<button type="button">바로가기</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h3 class="tab" onclick="location.href='index.php?mode=business&con=manu05'">개발사업</h3>
                <div class="tabmenu">
                    <div class="tab_inner">
                        <div class="img_info">
                            <div class="img_wrap">
                                <img src="template/images/business/b_list_img5.png" alt=""/>
                            </div>
                            <div class="info_wrap">
                                <div class="cover">
                                    <div class="cover_inner">
                                        <h3>개발사업</h3>
                                        <p>
                                            대림코퍼레이션은 석유화학 트레이딩, 물류·해운 서비스의
                                            기존 사업 경쟁력을 기반으로 미래 성장동력이 될 신규 사업을
                                            적극 개발 및 추진하고 있습니다.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_business_wrap">
                            <ul class="development_work">
                                <li>개발사업</li>
                                <li>개발사업<button type="button">바로가기</button></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

    </div>
</div>

<script>
tab_on(document.querySelectorAll('.tab'),'onClick','con');//tab기능
</script>