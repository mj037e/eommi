$(document).ready(function() {

	var num = 1;
	var m_start;

	var slide = $('div.slide li');
	var show = $('div.show li');

	move_start();

	$('div.show li').hover(enter,leave);

	function move_start(){

		m_start = setInterval(function(){

			slide.hide();			
			slide.eq(num).show();
			show.eq(num).addClass('selected');
			show.eq(num).siblings().removeClass('selected');

			num = num + 1

			if ( num > 4 ) {
				num = 0;
			}
		}, 2000);
	}

	function move_stop(){

	 clearInterval(m_start);   
	}

	function leave(){
		
		move_start();
	}

	function enter(){

		move_stop();

		var index = $(this).index();

		slide.hide();
		slide.eq(index).show();
		show.eq(index).addClass('selected');
		show.eq(index).siblings().removeClass('selected');


	}

});