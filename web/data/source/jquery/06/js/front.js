$(document).ready(function() {

	$('ul li').eq(1).click(function(){
		$('div.slide img').first().appendTo('div.slide');
		$('div.slide img').removeClass('big');
		$('div.slide img').eq(2).addClass('big');
	});
	
	$('ul li').eq(0).click(function(){
		$('div.slide img').last().prependTo('div.slide');
		$('div.slide img').removeClass('big');
		$('div.slide img').eq(2).addClass('big');
	});

});