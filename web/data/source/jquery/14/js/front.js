$(document).ready(function() {

	var color = ['#de5e5e', '#51a2ed', '#d279cb', '#e9b34b', '#60ba5b' ];
	var title = ['IRONMAN', 'CAPTAIN', 'THOR', 'SPIDER', 'HULK' ];

	$('.gnb li').each( function(index) {
		$(this).delay(index*100).animate({'top':0}, 2000, 'easeOutElastic');
	});

	$('.gnb li').hover( function() {
		$(this).children('h2').toggleClass('hover', 200);	
	});

	$('.gnb li').click( function() {
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
		var index = $(this).index();
		$('body').animate({'background-color': color[index] });
		
		$('.title').text( title[index] );
	});

	var b = 0;

	setInterval(function(){


		$('ul.gnb li').eq(b).trigger('mouseenter');
		$('ul.gnb li').eq(b).trigger('click');

		b=b+1;

		if ( b > 4 ) {
			b=0;
		}
	},2000);

});