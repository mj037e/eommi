var doc = document;

// click 이벤트
var eventbox1 = doc.querySelector('.event1');//선택자(태그)

eventbox1.addEventListener('click',function(){
    this.classList.add('up'); 
});

//mouseover/mouseout
var eventbox2 = doc.querySelector('.event2');//선택자(태그)

eventbox2.addEventListener('mouseover',function(){
   this.classList.add('up'); 
});
eventbox2.addEventListener('mouseout',function(){
   this.classList.remove('up');
});

//mousedown/mouseup
var eventbox3 = doc.querySelector('.event3');//선택자(태그)

eventbox3.addEventListener('mousedown',function(){
   this.classList.add('up'); 
});
eventbox3.addEventListener('mouseup',function(){
   this.classList.remove('up');
});


//keydown/keyup
var eventbox4 = doc.querySelector('.event4');//선택자(태그)
window.addEventListener('keydown',function(event){
    //console.log(event.keyCode);
    if(event.keyCode == 38 ){
        eventbox4.classList.add('up'); 
    }
});

window.addEventListener('keyup',function(event){
    if(event.keyCode == 40 ){
        eventbox4.classList.remove('up'); 
    }
});


//window - scroll
var eventbox5 = doc.querySelector('.event5');//선택자(태그)

window.addEventListener('scroll',function(){
    
    //console.log(window.scrollY);
    console.log(document.body.scrollTop);
    
    var position = document.body.scrollTop;
    
    if( position > 300 ){
    eventbox5.classList.add('color');        
    }
    
    if( position > 600 ){
    eventbox5.classList.add('up');        
    }

});

//window - resize(반응형할때 필요한)
var eventbox6 = doc.querySelector('.event6');//선택자(태그)

window.addEventListener('resize', function(){
    
    //console.log(window.innerWidth);
    //eventbox6.classList.add('color');
    if( window.innerWidth <400){
        eventbox6.classList.add('color');
    } else{
    eventbox6.classList.remove('color');        
    }
    
});

/*$(window).on('resize',function(){
    
    //console.log($(window).width()); 
    
    if($(window).width() < 400){
        $('.event6').addClass('nav');
    } else{
        $('.event6').removeClass('nav');
    }
    
});*/



//애니메이션 구현하기

/*
$('.box').animate({ 
    //'margin-left' : '200px' , 
    'left' : '200px' ,
    'width' : '200px' 
});
*/

//자바스크립트로 애니메이션
//setInterval (반복 기능을 하는 함수)
//setInterval(function(){},3000);
//setInterval(동작,시간);
//매 "시간"마다 "동작"을 수행하는 기능
//1000ms  -> 1초

var num = 1;
/* 
setInterval(function(){
    
    console.log(num + '번째 실행');
    num++;
    
},3000);
*/


//변수의 위치(전역, 지역)
var startBtn = doc.getElementById('start');
var stopBtn = doc.getElementById('stop');
var aniBox = doc.querySelector('.box')


//전역변수
var motion;
var limit = 200;

startBtn.addEventListener('click',function(){
    
    //지역변수
    motion = setInterval(function(){
        if (num > limit ){
            clearInterval(motion);
        }
        aniBox.style.left = num + 'px';
        num++
    },10);
});

stopBtn.addEventListener('click',function(){
    clearInterval(motion);
    aniBox.style.left = '0px';
    num = 0;
});

/*
$('.box').on('click',function(){
    $('.box').fadeOut();
    
});
*/
















































