$(document).ready(function() {

  $('div.top').click(function(event){

    event.preventDefault();

    $('html, body').animate({'scrollTop':0});
    
  });
	
	$(".scroll").click(function(event){
		
    event.preventDefault();

    var index = $(this).parent().index();
    var thisSection = $('.section').eq(index);

    var pos = thisSection.position().top;

    $('html, body').animate({'scrollTop':pos});
	});

	var coverHeight = $('div.nav').position().top;

	$(window).on('scroll', function() {
    
    var nowScroll = $(window).scrollTop();

    if( nowScroll >= coverHeight ) {
      $('div.nav').addClass('fixed');
    } else {
      $('div.nav').removeClass('fixed');
      $('div.nav a').removeClass('active');
    }

    $('.section').each( function(index) {
      
      var posStart = $(this).position().top;
      var posEnd = posStart + $(this).height();

      if( nowScroll >= posStart && nowScroll < posEnd ) {
        $('div.nav li').eq(index).children('a').addClass('active');             
        $('div.nav li').eq(index).siblings().children('a').removeClass('active');
      }
    });

	});

});