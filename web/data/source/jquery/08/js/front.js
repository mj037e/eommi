$(document).ready(function() {

	$('.tabs li h2').on({

		click: function() {
			$(this).addClass('open');
			$(this).parent().siblings().children('h2').removeClass('open');
			$(this).next().show();
			$(this).parent().siblings().children('p').hide();
		}, 

		mouseover: function() {
			$(this).css('color','#048ffb');
		}, 

		mouseout: function() {
			$(this).css('color','#666');
		}
	});

	// $('.tabs li h2').eq(0).trigger('click');

});