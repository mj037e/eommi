document.addEventListener('DOMContentLoaded',function(){

    location_hash();
    //header gnb list
    (function(){
        var gnb_list = ['page1','page2','page3','page4','page5'];

        var i = 0;
        var tags = [];
        var li = '';
        while(i<gnb_list.length){
            li = document.createElement('li');
            li.innerHTML = '<a href="#!'+gnb_list[i]+'" onclick="fetchPage(\'./view/'+gnb_list[i]+'.html\')">'+gnb_list[i]+'</a>';
            tags[i] = li;
            document.querySelector('.gnb ul').appendChild(tags[i]);
            i = i + 1;
        };
    })();

});

window.addEventListener("hashchange",function(){
    location_hash();
});

function fetchPage(name){
    fetch(name).then(function(response){
        response.text().then(function(text){
            // console.log(text);
            document.querySelector('.container .section').innerHTML = text;
        })
    });
}
function location_hash(){
    if(location.hash){
        fetchPage('./view/'+location.hash.substr(2)+'.html');
    }else{
        fetchPage('./view/main.html');
    }
}

